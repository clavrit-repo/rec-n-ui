const BottomWidgets = () => {
    return ( 
        <div className="row mt-5 justify-content-center">
              <div className="col-xl-3 col-lg-4 col-md-6">
                <div className="registerBlocks opacity-75">
                  <ul className="w-100 d-flex justify-content-between align-items-center p-4 shadow bg-white mx-auto">
                    <li className="w-25">
                      <img src="images/campus_Img.svg" alt="" />
                    </li>
                    <li className="w-75 d-flex justify-content-end">
                      <h2 className="font-weight-normal">25</h2>
                      <h5 className="text-muted fw-light ms-4">
                        Campus
                        <br />
                        Registered
                      </h5>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-md-6 opacity-75">
                <div className="registerBlocks">
                  <ul className="w-100 d-flex justify-content-between align-items-center p-4 shadow bg-white mx-auto">
                    <li className="w-25">
                      <img src="images/student_Img.svg" alt="" />
                    </li>
                    <li className="w-75 d-flex justify-content-end">
                      <h2 className="font-weight-normal">5K</h2>
                      <h5 className="text-muted fw-light ms-4">
                        Student
                        <br />
                        Registered
                      </h5>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-md-6 opacity-75">
                <div className="registerBlocks">
                  <ul className="w-100 d-flex justify-content-between align-items-center p-4 shadow bg-white mx-auto">
                    <li className="w-25">
                      <img src="images/company_Img.svg" alt="" />
                    </li>
                    <li className="w-75 d-flex justify-content-end">
                      <h2 className="font-weight-normal">10</h2>
                      <h5 className="text-muted fw-light ms-4">
                        Company
                        <br />
                        Registered
                      </h5>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
     );
}
 
export default BottomWidgets;