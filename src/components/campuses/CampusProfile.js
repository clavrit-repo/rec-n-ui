import axios from "axios";
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import authService from "../../services/auth-service";
import { API_URL } from "../../services/auth-service";
import { AboutView } from "../company/NewsView";
import NewsView from "./news/NewsView";

import StudentGroupByStream from "./student/StudentGroupByStream";
import ProfileView from "./profile/ProfileView";
import AddCompanyModel from "./company/AddCompanyModal";
import EditCompany from "./company/EditCompany";
import ViewCompanies from "./company/ViewCompanies";
import StudentList from "./student/StudentList";

const CompaniesView = function () {
  return (
    <div className="col-xl-12 col-lg-12 col-md-12 mt-5">
      <div className="card shadow border-0 pt-3 h-100">
        <div className="card-body">
          <div className="d-flex justify-content-between">
            <h4 className="fs-5 fw-light">
              <span className="textblue">3</span> Companies Visited
            </h4>
            <div className="fs-6">
              <a href="#" className="text-muted">
                View All
              </a>
            </div>
          </div>
          <div className="row justify-content-between my-3">
            {/*  {companyWidget()} */}

            <div className="col-lg-4 col-md-12 mt-lg-0 mt-3 d-flex justify-content-start align-items-center">
              <img src="images/blank.svg" alt className="box-width" />
              <p className="fs-6 ms-3 mb-0 text-dark">
                HCL
                <br />
                Technologies
              </p>
            </div>
            <div className="col-lg-4 col-md-12 mt-lg-0 mt-3 d-flex justify-content-start align-items-center">
              <img src="images/blank.svg" alt className="box-width" />
              <p className="fs-6 ms-3 mb-0 text-dark">
                Infosys Information technology
              </p>
            </div>
            <div className="col-lg-4 col-md-12 mt-lg-0 mt-3 d-flex justify-content-start align-items-center">
              <img src="images/blank.svg" alt className="box-width" />
              <p className="fs-6 ms-3 mb-0 text-dark">
                Tata
                <br />
                Consultansy Services
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const CampusProfile = (props) => {
  const { loggedIn } = props;
  console.log("user status:", loggedIn);
  const navigate = useNavigate();

  const [isLogin] = useState(async () => {
    //console.log("AUTH SERVICE ========", await authService.getLogin());
    return await authService.getLogin();
  });

  const [campus, setCampus] = useState(null);
  const [students, setStudents] = useState(null);
  const [companies, setCompanies] = useState(null);
  const [shareCompany, setShareCompany] = useState(null);

  useEffect(() => {
    if (isLogin == null) {
      navigate("/");
    } else {
      authService.getCampusUser().then((user) => {
        setCampus(user);
        getCompanies();
        campusStudents();
      });
    }
  }, []);

  function campusStudents() {
    /*  axios.get(API_URL + "users?=campusId=2&" + campus.id).then((response) => {
      setStudents(response.data.data);
    }); */
    axios.get("http://localhost:3004/users/").then((response) => {
      debugger;
      setStudents(response.data);
    });
  }

  function companyWidget(companies) {
    if (!companies) return false;
    else {
      return companies.map((company) => {
        return (
          <div className="col-lg-4 col-md-12 mt-lg-0 mt-3 d-flex justify-content-start align-items-center">
            <img src="images/blank.svg" alt className="box-width" />
            <p className="fs-6 ms-3 mb-0 text-dark">{company.name}</p>
          </div>
        );
      });
    }
  }

  /** ADD */
  function addCompany(item) {
    debugger;
    axios
      .post("http://localhost:3004/campus_companies/", item)
      .then((response) => {
        getCompanies();
      });
  }
  /** Delete */
  function deleteCompany(id) {
    debugger;
    axios
      .delete("http://localhost:3004/campus_companies/" + id)
      .then((response) => {
        getCompanies();
      });
  }

  /**Update */
  function updateShareCompany(company) {
    setShareCompany(company);
  }
  function onUpdateCompanyPost(id, item) {
    return axios
      .put("http://localhost:3004/campus_companies/" + id, item)
      .then((response) => {
        getCompanies();
        return response.data;
      });
  }

  const getCompanies = function () {
    axios
      .get("http://localhost:3004/campus_companies")
      .then((response) => {
        return response.data;
      })
      .then((pcampus) => {
        setCompanies(pcampus);
      });
  };

  function onUpdateCompany() {}

  if (!campus) return null;
  else {
    return (
      <main className="bg-light">
        <section className="profileSec px-xl-5 px-lg-5 py-5">
          <div className="container-fluid">
            <div className="row profileView">
              <div className="col-xl-3 col-lg-6 col-md-6 mt-5">
                <ProfileView campus={campus} />
              </div>
              <div className="col-xl-6 col-lg-6 col-md-6">
                <div className="row">
                  {/*     <CompaniesView companies={campus.companies} /> */}

                  <ViewCompanies
                    companies={companies}
                    onDelete={deleteCompany}
                    onUpdate={updateShareCompany}
                  />
                  <StudentGroupByStream />
                  <AboutView />
                  <NewsView />
                  <AddCompanyModel onAdd={addCompany} />
                  <EditCompany
                    onUpdateCompany={onUpdateCompanyPost}
                    shareCompany={shareCompany}
                  />
                </div>
              </div>
              <StudentList students={students} />
            </div>
          </div>
        </section>
      </main>
    );
  }
};
export default CampusProfile;

const CampusAddModel = (props) => {
  debugger;
  const { getCompany } = props;
  const [campus, setCampus] = useState("");
  return (
    <div
      className="modal fade"
      id="campus-add-model"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabIndex={-1}
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content rounded-0">
          <div className="modal-header border-0">
            <h5 className="modal-title" id="staticBackdropLabel">
              Add New Company
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            />
          </div>
          <div className="modal-body px-4">
            <form className="row">
              <div className="col-md-12 position-relative mt-lg-2 mt-5 ">
                <label
                  htmlFor="validationCustom01"
                  className="form-label fw-normal text-muted fs-6 ms-3 mb-0 text-dark"
                >
                  Company Name
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  id="val"
                  placeholder=""
                  onChange={(e) => setCampus(e.target.value)}
                />
              </div>
              <div className="col-12 d-flex my-5">
                <button
                  className="btn btn-outline-secondary w-50 rounded-pill me-1"
                  data-bs-dismiss="modal"
                  type="button"
                >
                  Cancel
                </button>
                <button
                  className="btn btn-primary w-50 rounded-pill ms-1"
                  type="button"
                  data-bs-dismiss="modal"
                  onClick={() =>
                    getCompany({
                      name: campus,
                    })
                  }
                >
                  Register
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

const UpdateCampus = (props) => {
  debugger;
  const { onUpdateCampusPost, shareCampus } = props;
  const [campusName, setCampusName] = useState(shareCampus?.name);
  const [invalid, setInvalid] = useState(false);
  const [msg, setMsg] = useState(false);

  useEffect(() => {
    setCampusName(shareCampus?.name);
  }, [shareCampus]);

  const onSubmit = () => {
    debugger;
    if (!campusName) {
      setInvalid(true);
      setMsg(false);
    } else {
      onUpdateCampusPost(shareCampus.id, {
        name: campusName,
      }).then((result) => {
        setCampusName(""); // set to defualt
        setInvalid(false);
        setMsg("Campus Updated Successfully!");
      });
      //UpdateCampus(campus).then((result) => {});
    }
  };

  return (
    <div
      className="modal fade"
      id="campus-update-modal"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabIndex={-1}
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content rounded-0">
          <div className="modal-header border-0">
            <h5 className="modal-title" id="staticBackdropLabel">
              Update Company
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            />
          </div>
          <div className="modal-body px-4">
            {msg && <p className="text-danger text-center">{msg}</p>}
            <form className="row">
              <div className="col-md-12 position-relative mt-lg-2 mt-5 ">
                <label
                  htmlFor="validationCustom01"
                  className="form-label fw-normal text-muted"
                >
                  Campus Name
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  id="val"
                  placeholder=""
                  value={campusName}
                  onChange={(e) => setCampusName(e.target.value)}
                />
              </div>
              {invalid && !campusName && (
                <p className="text-danger">This field is required</p>
              )}
              <div className="col-12 d-flex my-5">
                <button
                  className="btn btn-outline-secondary w-50 rounded-pill me-1"
                  data-bs-dismiss="modal"
                  type="button"
                >
                  Cancel
                </button>
                <button
                  className="btn btn-primary w-50 rounded-pill ms-1"
                  type="button"
                  onClick={onSubmit}
                >
                  Update
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
