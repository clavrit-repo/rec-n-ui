import axios from "axios";
import { useEffect, useState } from "react";
import ActionNode from "../ActionNode";
import authService from "../../../services/auth-service";
import { API_URL } from "../../../services/auth-service";
import { flashMsg, setLabel, soptions, statusClass } from "../commonFunctions";

const CertificateNode = (props) => {
  const { name, score } = props.data;
  const { id, onDelete, onUpdate } = props;
  return (
    <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
      <div className="w-75">
        <h6>{name}</h6>
        <h6 className={statusClass(score)}>{setLabel(score)}</h6>
      </div>
      <ActionNode
        type="certificate"
        onDelete={onDelete}
        onUpdate={onUpdate}
        id={id}
      />
    </div>
  );
};

const Certificates = (props) => {
  //const user=authService.getCurrentUser();
  const [user, setUser] = useState(props.user);
  const [msg, setMsg] = useState("");
  const [userCertificates, setUserCertificates] = useState(user.certificates);

  const [name, setName] = useState("");
  const [score, setScore] = useState("");
  const [isUpdate, setIsUpdate] = useState(false);
  const [isNotValid, setIsNotValid] = useState(false);

  useEffect(() => {
    debugger;
    getCertificates();
  }, []);

  function getCertificates() {
    axios.get(API_URL + `users/${user.id}/certificate`).then((response) => {
      if (response.status == 200 || response.data.code == 200) {
        setUserCertificates(response.data.data);
      }
    });
  }

  function addCertificate({ name, score }) {
    if (!name || !score) {
      setIsNotValid(1);
      return;
    }

    axios
      .post(API_URL + `users/${user.id}/certificate`, {
        name,
        score,
        userId: user.id,
      })
      .then((response) => {
        if (response.status == 201 || response.data.code == 200) {
          setMsg("Certificate Inserted Successfully !!!");
          setDefaults();
          getCertificates();
        }
      })
      .catch((error) => {
        console.log(error.message);
      })
      .finally(hideAlert);
  }

  const deleteItem = (id) => {
    //axios.post(API_URL + `users/${user.id}/del_certificate/` + id + "/");
    axios
      .delete(API_URL + `certificate/` + id + "/")
      .then((response) => {
        if (response.status == 200 || response.data.code == 200) {
          setMsg("Certificate Delted successfully !!!");
          setDefaults();
          getCertificates();
        }
      })
      .catch((error) => {
        console.log(error.message);
      })
      .finally(hideAlert);
  };

  function updateItem(item) {
    debugger;
    const { id, name, score } = item;
    setName(name);
    setScore(score);
    setIsUpdate(id);
  }

  function setDefaults() {
    setName("");
    setScore("");
    setIsUpdate(false);
    setIsNotValid(false);
  }

  const hideAlert = () => {
    window.setTimeout(() => {
      setMsg("");
    }, 4000);
  };

  function updateCertificate(id, item) {
    debugger;
    const nitem = { id, ...item, userId: user.id };
    axios
      .put(API_URL + `certificate/${id}`, nitem)
      .then((response) => {
        if (response.status == 200 || response.data.code == 200) {
          setMsg("Certificate Updated Successfully !!!");
          setDefaults();
          getCertificates();
        }
      })
      .catch((error) => {
        console.log(error.message);
      })
      .finally(hideAlert);
  }

  return (
    <div id="row3" className="mb-5">
      <div className="card shadow border-0 py-3 px-2">
        <div className="card-body">
          {msg && flashMsg(msg)}
          <div className="blockMob d-flex justify-content-between align-items-start">
            <div className="">
              <h5 className="text-dark fw-light mb-4">Certification</h5>
              <form id="Cform" className="row needs-validation" novalidate>
                <div className="col-md-12 mt-3">
                  <label
                    htmlFor="validationCustomUsername"
                    className="form-label fw-normal text-muted"
                  >
                    Certification Name
                  </label>
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control fw-normal text-sece py-2"
                      placeholder="Enter Name"
                      onChange={(e) => setName(e.target.value)}
                      value={name}
                      required
                    />
                  </div>
                  {!name && isNotValid && (
                    <span className="text-danger">This field is required</span>
                  )}
                </div>
                <div className="col-md-12 mt-5">
                  <label
                    htmlFor="validationCustomUsername"
                    className="form-label fw-normal text-muted"
                  >
                    Certification Status
                  </label>
                  <div className="input-group">
                    <select
                      className="form-select text-dark fw-normal py-2"
                      aria-label="Default select example"
                      onChange={(e) => setScore(e.target.value)}
                      value={score}
                      required
                    >
                      <option value="">Select Status</option>
                      {soptions.map((i, idx) => (
                        <option key={idx} value={i.id}>
                          {i.text}
                        </option>
                      ))}
                    </select>
                  </div>
                  {!score && isNotValid && (
                    <span className="text-danger">This field is required</span>
                  )}
                  {!isUpdate && (
                    <button
                      className="btn btn-primary w-100 rounded-pill ms-1 py-2 mt-4 fs-5"
                      type="button"
                      onClick={() => addCertificate({ name: name, score })}
                    >
                      Add
                    </button>
                  )}

                  {isUpdate && (
                    <span>
                      <button
                        className="btn btn-primary w-20 rounded-pill ms-1 py-2 mt-4 fs-5"
                        type="button"
                        style={{ width: "30%" }}
                        onClick={() =>
                          updateCertificate(isUpdate, {
                            name,
                            score,
                          })
                        }
                      >
                        Update
                      </button>
                      <button
                        style={{ width: "40%" }}
                        className="btn btn-info w-30  rounded-pill ms-1 py-2 mt-4 fs-5"
                        type="button"
                        onClick={setDefaults}
                      >
                        Cancel Update
                      </button>
                    </span>
                  )}
                </div>
              </form>
            </div>
            <div className="w-50 rightblockMob">
              {userCertificates &&
                userCertificates.map((certificate, idx) => (
                  <CertificateNode
                    key={idx}
                    id={certificate.id}
                    data={certificate}
                    onDelete={() => deleteItem(certificate.id)}
                    onUpdate={() => updateItem(certificate)}
                  />
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Certificates;
