import { useEffect, useState } from "react";

const EditCompany = (props) => {
  //debugger;
  const { shareCompany, onUpdateCompany: doUpdate } = props;
  const [jobtitle, setJobTitle] = useState(shareCompany?.jobtitle);
  const [company, setCompany] = useState(shareCompany?.company);
  const [email, setEmail] = useState(shareCompany?.email);
  const [phone, setPhone] = useState(shareCompany?.phone);
  const [invalid, setInvalid] = useState(false);
  const [msg, setMsg] = useState(false);

  useEffect(() => {
    setJobTitle(shareCompany?.jobtitle);
    setCompany(shareCompany?.company);
    setEmail(shareCompany?.email);
    setPhone(shareCompany?.phone);
  }, [shareCompany]);

  const onSubmit = function (item) {
    //debugger;
    if (!jobtitle || !company || !email || !phone) {
      console.log("validation Error");
      setInvalid(true);
      setMsg("");
    } else {
      doUpdate(shareCompany.id, {
        jobtitle,
        company,
        email,
        phone,
      }).then((result) => {
        setJobTitle("");
        setCompany("");
        setEmail("");
        setPhone("");
        setInvalid(false);
        setMsg("Company Info Updated!!");
      });
    }
  };

  return (
    <div
      className="modal fade"
      id="job-update-modal"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabIndex={-1}
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content rounded-0">
          <div className="modal-header border-0">
            <h5 className="modal-title" id="staticBackdropLabel">
              Update Opening
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            />
          </div>
          <div className="modal-body px-4">
            <form className="row">
              {msg && <p className="text-danger text-center">{msg}</p>}
              <div className="col-md-6 position-relative mt-lg-2 mt-5 ">
                <label
                  htmlFor="validationCustom01"
                  className="form-label fw-normal text-muted"
                >
                  Job Title
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  id="validationCustom01"
                  placeholder=""
                  value={jobtitle}
                  onChange={(e) => setJobTitle(e.target.value)}
                />
                {invalid && !jobtitle && (
                  <p className="text-danger">This field is required</p>
                )}
              </div>
              <div className="col-md-6 mt-lg-2 mt-5">
                <label
                  htmlFor="validationCustom02"
                  className="form-label fw-normal text-muted"
                >
                  company Required
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  id="validationCustom02"
                  placeholder=""
                  value={company}
                  onChange={(e) => setCompany(e.target.value)}
                />
                {invalid && !company && (
                  <p className="text-danger">This field is required</p>
                )}
              </div>
              <div className="col-md-6 mt-5">
                <label
                  htmlFor="validationCustom02"
                  className="form-label fw-normal text-muted"
                >
                  Expected offered package
                </label>
                <input
                  type="email"
                  className="form-control fw-bold text-dark"
                  placeholder=""
                  value={phone}
                  onChange={(e) => setPhone(e.target.value)}
                />
                {invalid && !phone && (
                  <p className="text-danger">This field is required</p>
                )}
              </div>
              <div className="col-md-6 mt-5">
                <label
                  htmlFor="validationCustom02"
                  className="form-label fw-normal text-muted"
                >
                  Expected email
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  placeholder=""
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                {invalid && !email && (
                  <p className="text-danger">This field is required</p>
                )}
              </div>
              <div className="col-12 d-flex my-5">
                <button
                  className="btn btn-outline-secondary w-50 rounded-pill me-1"
                  data-bs-dismiss="modal"
                  type="button"
                >
                  Cancel
                </button>
                <button
                  className="btn btn-primary w-50 rounded-pill ms-1"
                  type="button"
                  /*  data-bs-dismiss="modal" */
                  onClick={() =>
                    onSubmit({
                      jobtitle,
                      company,
                      email,
                      phone,
                    })
                  }
                >
                  Update Company
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditCompany;
