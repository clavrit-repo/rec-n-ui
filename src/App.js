import logo from "./logo.svg";
import "./App.css";
import React, { useEffect, useState } from "react";
import Home from "./components/Home";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  useNavigate,
} from "react-router-dom";

import StudentRegister from "./components/student/StudentRegister";
import StudentLogin from "./components/StudentLogin";
import StudentProfile from "./components/student/StudentProfile";
import authService from "./services/auth-service";
import StudentUpdateProfile from "./components/student/StudentUpdateProfile";
import CompanyProfile from "./components/company/CompanyProfile";
import CompanyRegister from "./components/company/ComapnyRegister";

import CampusProfile from "./components/campuses/CampusProfile";
import CampusRegister from "./components/campuses/CampusRegister";
import TopHead from "./components/includes/TopHead";
import ClassComponent from "./components/ClassComponent";
import CampusProfileUpdate from "./components/campuses/CampusProfileUpdate";
//import { scryRenderedDOMComponentsWithClass } from "react-dom/cjs/react-dom-test-utils.production.min";

import { useSelector } from "react-redux";
import { selectUser } from "./services/userSlice";

function App() {
  const navigate = new useNavigate();
  const isuser = useSelector(selectUser);
  const [loggedIn, setLoggedIn] = useState(authService.checkLogin);

  //const [checkLogin, setCheckLogin] = useState(authService.checkLogin());

  useEffect(() => {
    const user = authService.getCurrentUser();
    if (user) {
      setLoggedIn(true);
    } else {
      navigate("/");
    }
  }, [loggedIn]);

  function logOut() {
    authService.logout();
    navigate("/");
  }

  return (
    <main>
      <React.Fragment>
        <TopHead loggedIn={loggedIn} logout={logOut} />
      </React.Fragment>
      <Routes>
        <Route path="/" exact element={<Home loggedIn={loggedIn} />}></Route>
        <Route path="/sign-in" element={<StudentLogin />}></Route>
        <Route
          path="/student-register"
          element={<StudentRegister loggedIn={loggedIn} />}
        ></Route>
        <Route
          path="/student-profile/:id"
          element={<StudentProfile loggedIn={loggedIn} />}
        ></Route>
        <Route
          path="/student-profile"
          element={<StudentProfile loggedIn={loggedIn} />}
        ></Route>
        <Route
          path="/manage-profile"
          element={<StudentUpdateProfile loggedIn={loggedIn} />}
        ></Route>
        <Route
          path="/company-register"
          element={<CompanyRegister loggedIn={loggedIn} />}
        ></Route>
        <Route
          path="/company-profile/:id"
          element={<CompanyProfile loggedIn={loggedIn} />}
        ></Route>
        <Route
          path="/company-profile/:id"
          element={<CompanyProfile loggedIn={loggedIn} />}
        ></Route>

        <Route path="/campus-register" element={<CampusRegister />}></Route>
        <Route path="/campus-profile/:id" element={<CampusProfile />}></Route>
        <Route path="/campus-profile" element={<CampusProfile />}></Route>
        <Route path="/campus-update" element={<CampusProfileUpdate />}></Route>
      </Routes>
    </main>
  );
}
export default App;
