import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
  name: "user",
  initialState: {
    user: false,
  },
  reducers: {
    userx: (state) => {
      const userStr = localStorage.getItem("user");
      if (userStr != null) {
        state.user = true;
      } else {
        state.user = false;
      }
    },
  },
});

export const { userx } = userSlice.actions;
export const selectUser = (state) => state.user.user;
export default userSlice.reducer;
