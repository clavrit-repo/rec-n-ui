import { useForm } from "react-hook-form";
import HeadAfterLogin from "../includes/HeadAfterLogin";
import authService, { API_URL } from "../../services/auth-service";
import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import TopHead from "../includes/TopHead";
import { sClass, sPercentage, statusClass } from "./commonFunctions";
import axios from "axios";
import { CChart } from "@coreui/react-chartjs";

const getSkills = function (skills) {
  return skills.map((skill, idx) => {
    return (
      <button
        key={idx}
        type="button"
        className="btn btn-common btn-sm text-dark px-3 me-2"
      >
        {skill.name}
      </button>
    );
  });
};

const getIntruests = function (interests) {
  return interests.map((interest, idx) => {
    return (
      <button
        key={idx}
        type="button"
        className="btn btn-common btn-sm text-dark px-3 me-2"
      >
        {interest.name}
      </button>
    );
  });
};

const getProjects = function (projects) {
  return projects?.map((project, idx) => {
    return (
      <div
        key={idx}
        className="d-flex align-items-center success_rateBlock my-4"
      >
        <div
          className="fs-5  circleImg circleImg2
                position-relative "
        >
          <span className="countper position-absolute">
            {/*  {Math.floor(100 * Math.random()) + "%"} */}
            {project.score + "%"}
          </span>
        </div>
        <div className="">
          <h6 className="text-dark fw-light">{project.name}</h6>
        </div>
      </div>
    );
  });
};

const getCoursesView = function (courses) {
  return courses.map((course) => {
    return (
      <div className="progressbox my-5">
        <h6>{course.name}</h6>
        <h6>Course in Progress</h6>
        <div className="progress mt-3" style={{ height: "12px" }}>
          <div
            className={"progress-bar bg-" + sClass(course.score)}
            role="progressbar"
            style={{ width: sPercentage(course.score) + "%" }}
          ></div>
        </div>
      </div>
    );
  });
};

const CertificateView = (props) => {
  //debugger;
  const { certificates } = props;

  if (!certificates) return null;
  else
    return certificates.map((certificate) => {
      return (
        <div className="progressbox my-5">
          <h6>{certificate.name}</h6>
          <h6>Course in Progress</h6>
          <div className="progress mt-3" style={{ height: "12px" }}>
            <div
              className="progress-bar bg-warning"
              role="progressbar"
              style={{ width: sPercentage(certificate.score) + "%" }}
              aria-valuenow="50"
              aria-valuemin="0"
              aria-valuemax="100"
            ></div>
          </div>
        </div>
      );
    });
};

const SkillView = function (props) {
  const { skills } = props;
  return (
    <div className="my-5">
      <h5 className="text-dark fw-light mb-3">Skills</h5>
      {skills &&
        skills.split(",").map((skill, idx) => {
          return (
            <button
              key={idx}
              type="button"
              className="btn btn-common btn-sm text-dark px-3 me-2"
            >
              {skill}
            </button>
          );
        })}
    </div>
  );
};

const InterestView = function (props) {
  const { interests } = props;
  return (
    <div className="my-5">
      <h5 className="text-dark fw-light mb-3">Interest</h5>
      {interests &&
        interests.split(",").map((interest, idx) => {
          return (
            <button
              key={idx}
              type="button"
              className="btn btn-common btn-sm text-dark px-3 me-2"
            >
              {interest}
            </button>
          );
        })}
    </div>
  );
};

const AboutView = function () {
  return (
    <div className="col-xl-12 col-lg-12 col-md-12 mt-4">
      <div className="card shadow  border-0 py-3 px-2 h-100">
        <div className="card-body">
          <h4 className="text-dark fw-light"> About</h4>
          <div className="mt-3" id="menu1">
            <ul className="term-list p-0">
              <li className="term-item ">
                <p className="text-muted">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Laboriosam sint animi voluptatem deserunt rem illum accusamus,
                  nostrum iste porro praesentium vel neque non consectetur
                  tempora laborum ea reprehenderit adipisci fugit?
                </p>
              </li>
              <li className="term-item ">
                <p className="moretext text-muted">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Laboriosam sint animi voluptatem deserunt rem illum accusamus,
                  nostrum iste porro praesentium vel neque non consectetur
                  tempora laborum ea reprehenderit adipisci fugit?
                </p>
              </li>
              <li className="term-item">
                <a className="moreless-button justify-content-end text-muted">
                  ...see more
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

const QualificationView = function (props) {
  const { qualificationSubjects, qualificationMarks, qualificationAvgMarks } =
    props;
  debugger;
  console.log(qualificationSubjects);
  return (
    <div className="col-xl-4 col-lg-12 col-md-12 mt-5">
      <div className="card shadow  border-0 py-3 px-2 h-100">
        <div className="card-body">
          <h5> Qualification</h5>
          <h6>
            Average
            <span className="textblue fs-5">
              {qualificationAvgMarks.toFixed(2)}%
            </span>
          </h6>
          <figure className="mt-5" style={{ height: "600px", width: "600" }}>
            <CChart
              type="bar"
              data={{
                labels: qualificationSubjects,
                datasets: [
                  {
                    label: "Qualification Graph",
                    backgroundColor: "#0d6efd",
                    data: qualificationMarks,
                  },
                ],
              }}
              labels="months"
            />
          </figure>
        </div>
      </div>
    </div>
  );
};

function ProjectView(props) {
  const { projects } = props;
  return (
    <div className="col-xl-4 col-lg-12 col-md-12 mt-5">
      <div className="card shadow  border-0 py-3 px-2 h-100">
        <div className="card-body">
          <h5> Projects</h5>
          <h6>
            Success Rate <span className="textblue fs-5">81%</span>
          </h6>
          {projects && getProjects(projects)}
        </div>
      </div>
    </div>
  );
}

const StudentProfile = (props) => {
  //debugger;
  //alert(JSON.stringify(props));
  console.log("props", props.match);
  const { loggedIn } = props;
  console.log("user status:", loggedIn);

  const navigate = useNavigate();

  /* const [isLogin] = useState(async () => {
    return authService.getLogin();
  }); */
  const [isLogin] = useState(() => {
    return authService.getLogin();
  });

  const [user, setUser] = useState(() => {
    authService.getCurrentUser().then((user) => {
      setUser(user);
    });
  });

  /* const [user, setUser] = useState(async () => {
    debugger;
    const u = await authService.getCurrentUser();
    return u;
  }); */

  const [projects, setProjects] = useState(null);
  const [certificates, setCertificates] = useState(null);
  const [courses, setCourses] = useState(null);
  const [qualifications, setQualifications] = useState(null);
  const [qualificationSubjects, setQualificationSubjects] = useState([]);
  const [qualificationMarks, setQualificationMarks] = useState([]);
  const [qualificationAvgMarks, setQualificationAvgMarks] = useState(0);

  useEffect(() => {
    debugger;
    /*if (isLogin == null) {
      navigate("/");
    } else {
    } */
    if (user) {
      //getStreams();
      getQualifications();
      getProjects();
      getCertificates();
      getCourses();
    }
  }, [user]);

  function getCourses() {
    axios.get(API_URL + `users/${user.id}/course`).then((response) => {
      if (response.status == 200 || response.data.code == 200) {
        setCourses(response.data.data);
        //setCourses(response.data); for mock
      }
    });
  }

  function getQualifications() {
    var sum = 0;
    axios.get(API_URL + `users/${user.id}/qualification`).then((response) => {
      if (response.status == 200 || response.data.code == 200) {
        setQualifications(response.data.data);
        const subs = response.data.map((qua) => {
          return qua.name;
        });
        const scores = response.data.map((qua) => {
          sum += parseFloat(qua.score);
          return qua.score;
        });
        setQualificationSubjects(subs);
        setQualificationMarks(scores);
        setQualificationAvgMarks(sum / scores.length);
      }
    });
  }

  function getStreams() {
    axios.get(API_URL + `users/${user.id}/qualification`).then((response) => {
      if (response.status == 200 || response.data.code == 200) {
        //setStreams(response.data);
      }
    });
  }

  function getProjects() {
    axios.get(API_URL + `users/${user.id}/project`).then((response) => {
      if (response.status == 200 || response.data.code == 200) {
        setProjects(response.data.data);
      }
    });
  }

  function getCertificates() {
    axios.get(API_URL + `users/${user.id}/certificate`).then((response) => {
      if (response.status == 200 || response.data.code == 200) {
        setCertificates(response.data.data);
      }
    });
  }

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  //const history = useHistory();

  if (!user) return null;
  else {
    return (
      <section className="profileSec  px-xl-5 px-lg-5 pb-5 py-5">
        <div className="container-fluid">
          <div className="row profileView">
            <div className="col-xl-3 col-lg-6 col-md-6 mt-5 ">
              <div className="card shadow  border-0 py-3 px-2 h-100">
                <div className="card-body">
                  <div class="cardhead d-flex">
                    <figure class="m-0 me-3">
                      <img
                        src="/images/user_img.png"
                        alt=""
                        class="w-100 border border-5 rounded-circle"
                      />
                    </figure>
                    <div>
                      <div class="rank p-1 px-2 textblue mb-3">Rank #9</div>
                      <h6 class="text-dark fw-light">
                        {user.firstName + " " + user.lastName}
                      </h6>
                      <p class="text-muted m-0">B.Tech - Final Year</p>
                      <p class="text-muted">
                        {user.city}, {user.state}, {user.country}
                      </p>
                      <a href="#" class="text-primary">
                        <span class="me-2">
                          <img src="images/pw-icon.png" alt="" />
                        </span>
                        Contact info
                      </a>
                      <div className="mt-1">
                        <p className="text-muted mb-2">{user.emailId}</p>
                        <p className="text-muted mb-2">{user.phone}</p>
                      </div>
                    </div>
                  </div>
                  <div class="d-flex mt-3 mb-5 justify-content-between">
                    <button
                      class="btn btn-primary py-2 w-50 rounded-pill ms-1 btn-sm"
                      type="submit"
                    >
                      Call for Interview
                    </button>
                    <button class="dropdown bg-white border border-1 btn btn-common w-50 rounded-pill py-2 ms-2 fw-light btn-sm text-dark">
                      Download Resume
                    </button>
                  </div>
                  <div className="cardbody mt-5">
                    <div>
                      <h6 className="text-muted fw-light">Campus</h6>
                      <h4 className="textblue">
                        <a href="campus-registration.html" className="textblue">
                          {user.campus && user.campusInfo.campusName}
                        </a>
                      </h4>
                    </div>

                    <SkillView skills={user.skills} />
                    <InterestView interests={user.interests} />

                    {/*  <div className="my-5">
                            <h5 className="text-dark fw-light mb-3">Skills</h5>
                            {user.skills && getSkills(user.skills)}
                          </div>
                          <div className="my-5">
                            <h5 className="text-dark fw-light mb-3">Interest</h5>
                            {user.interests && getIntruests(user.interests)}
                          </div> */}
                  </div>
                </div>
                <div className="card-footer text-center bg-white border-0 text-muted text-small">
                  <p>
                    Member Since:{" "}
                    <span className="text-dark">August 18, 2021</span>
                  </p>
                </div>
              </div>
            </div>

            <div className="col-xl-9 col-lg-6 col-md-6">
              <div className="row">
                <div className="col-xl-4 col-lg-12 col-md-12 mt-5">
                  <div className="card shadow  border-0 py-3 px-2 h-100">
                    <div className="card-body navTab">
                      <ul
                        className="nav nav-pills mb-3 "
                        id="pills-tab"
                        role="tablist"
                      >
                        <li
                          className="nav-item border-bottom"
                          role="presentation"
                        >
                          <button
                            className="nav-link active rounded-0 text-dark px-4"
                            id="pills-home-tab"
                            data-bs-toggle="pill"
                            data-bs-target="#pills-home"
                            type="button"
                            role="tab"
                            aria-controls="pills-home"
                            aria-selected="true"
                          >
                            Courses
                          </button>
                        </li>
                        <li
                          className="nav-item border-bottom"
                          role="presentation"
                        >
                          <button
                            className="nav-link rounded-0 px-4"
                            id="pills-profile-tab"
                            data-bs-toggle="pill"
                            data-bs-target="#pills-profile"
                            type="button"
                            role="tab"
                            aria-controls="pills-profile"
                            aria-selected="false"
                          >
                            Certification
                          </button>
                        </li>
                      </ul>

                      <div className="tab-content" id="pills-tabContent">
                        <div
                          className="tab-pane fade show active"
                          id="pills-home"
                          role="tabpanel"
                          aria-labelledby="pills-home-tab"
                        >
                          {/*  {user.courses && getCourses(user.courses)} */}
                          {courses && getCoursesView(courses)}
                        </div>

                        <div
                          className="tab-pane fade"
                          id="pills-profile"
                          role="tabpanel"
                          aria-labelledby="pills-profile-tab"
                        >
                          <CertificateView certificates={certificates} />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <ProjectView projects={projects} />
                <QualificationView
                  qualificationSubjects={qualificationSubjects}
                  qualificationMarks={qualificationMarks}
                  qualificationAvgMarks={qualificationAvgMarks}
                />
                <AboutView />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
};

export default StudentProfile;
