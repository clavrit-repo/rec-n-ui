import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { userx } from "../../services/userSlice";
import { Link, useNavigate } from "react-router-dom";
import authService from "../../services/auth-service";
import { useSelector } from "react-redux";
import { selectUser } from "../../services/userSlice";

const TopHead = (props) => {
  const dispatch = useDispatch();
  const isuser = useSelector(selectUser);
  const navigate = useNavigate();
  const [searchKey, setSearchKey] = useState();
  const [searchType, setSearchType] = useState("student");
  const [searchItems, setSearchItems] = useState([]);
  const [newlist, setNewList] = useState("");

  const [isLogin, setLoggedIn] = useState(() => {
    return authService.getLogin();
  });

  /** FOR LOCAL  */
  /* const [isLogin, setLoggedIn] = useState(null);
  useEffect(() => {
    authService.getLogin().then((user) => setLoggedIn(user));
  }, []); */
  /** FOR LOCAL  */

  useEffect(() => {
    setLoggedIn(authService.getLogin());
  }, [props]);

  const mainSearch = (e) => {
    setSearchKey(e.target.value);
    authService.getSearchResult(e.target.value, searchType).then((response) => {
      let totalSearch = response.length;
      if (totalSearch > 0) {
        setSearchItems(response);
        setNewList("newlist");
      } else {
        setSearchItems([]);
        setNewList("");
      }
    });
  };

  function userlogOut() {
    authService.logout();
    dispatch(userx());
    navigate("/");
  }

  return (
    <header>
      {isLogin && (
        <nav className="navbar navbar-expand-lg navbar-light px-5 py-3 bg-white w-100 position-fixed shadow-sm">
          <div className="container-fluid justify-content-between">
            <div>
              <Link to="/" className="navbar-brand me-0">
                <img src="/images/logo.jpeg" alt="logo" className="w-75" />
              </Link>
            </div>
            <p className="m-0 text-warning">
              Pay 50 Rs. for creating your profile
            </p>
            <p>
              <span style={{ margin: "5px" }}>
                <input
                  value="student"
                  name="searchType"
                  type="radio"
                  defaultChecked={searchType === "student"}
                  onChange={(e) => setSearchType(e.target.value)}
                />
                Student
              </span>
              <span style={{ margin: "5px" }}>
                <input
                  value="campus"
                  name="searchType"
                  type="radio"
                  defaultChecked={searchType === "campus"}
                  onChange={(e) => setSearchType(e.target.value)}
                />
                Campus
              </span>
              <span style={{ margin: "5px" }}>
                <input
                  value="company"
                  name="searchType"
                  type="radio"
                  defaultChecked={searchType === "company"}
                  onChange={(e) => setSearchType(e.target.value)}
                />
                Company
              </span>
            </p>
            <div className="bg-white input-group border rounded-pill p-2 w-50 searchInput my-2 position-relative">
              <div className="input-group-prepend border-0">
                <button
                  id="button-addon4"
                  type="button"
                  className="btn btn-link text-muted"
                >
                  <img src="/images/search_icon.png" />
                </button>
              </div>
              <input
                type="search"
                placeholder="Search Campus, Student, Institution"
                aria-describedby="button-addon4"
                className="fw-bold form-control bg-none border-0 mr-1 pe-4 ps-1"
                onChange={(e) => mainSearch(e)}
                value={searchKey}
              />
              <span
                className="position-absolute end-0 me-4 pw-Icon click"
                data-row-id="dropdownMenuButton-1"
              >
                <img src="/images/filter_icon.png" alt="filter" />
              </span>
              <div
                className={
                  "w-100 card position-absolute mt-5 filter-tab list dropdownMenuButton-1 " +
                  newlist
                }
              >
                <div className="card-body">
                  <div className="d-flex">
                    <div
                      className="list-group nav flex-column me-3  w-100"
                      id="v-pills-tab"
                      role="tablist"
                      aria-orientation="vertical"
                    >
                      {searchItems.length == 0 && (
                        <Link
                          to={""}
                          className="text-start list-group-item nav-link nav-btn"
                          style={{ marginBottom: "2px" }}
                        >
                          No Data
                        </Link>
                      )}

                      {searchItems.map((item, index) => {
                        let label;
                        let link;
                        if (searchType == "campus") {
                          label = item.campusName;
                          link = "/campus-profile/" + item.id;
                        } else if (searchType == "student") {
                          label = item.firstName;
                          link = "/student-profile/" + item.id;
                        } else if (searchType == "company") {
                          label = item.company;
                          link = "/company-profile/" + item.id;
                        }
                        return (
                          <Link
                            to={link}
                            className="text-start list-group-item nav-link nav-btn"
                            style={{ marginBottom: "2px" }}
                          >
                            {label}
                          </Link>
                        );
                      })}
                    </div>
                    <div className="tab-content w-75" id="v-pills-tabContent">
                      <div
                        className="tab-pane fade show active"
                        id="v-pills-home"
                        role="tabpanel"
                        aria-labelledby="v-pills-home-tab"
                      >
                        {/* <form className="row">
                          <div className="col-md-12">
                            <label
                              htmlFor="validationCustom04"
                              className="form-label text-muted"
                            >
                              Country
                            </label>
                            <select className="form-select fw-normal py-2 text-dark">
                              <option value>India</option>
                              <option>...</option>
                            </select>
                          </div>
                          <div className="col-md-12 mt-5">
                            <label
                              htmlFor="validationCustom02"
                              className="form-label fw-normal text-muted"
                            >
                              City
                            </label>
                            <input
                              type="text"
                              className="form-control fw-normal py-2 text-dark"
                              placeholder="Gurgaon"
                            />
                          </div>
                          <div className="col-md-12 mt-5">
                            <button
                              className="fs-6 btn btn-primary w-100 rounded-pill py-2"
                              type="submit"
                            >
                              Search
                            </button>
                          </div>
      </form>*/}
                      </div>
                      <div
                        className="tab-pane fade"
                        id="v-pills-profile"
                        role="tabpanel"
                        aria-labelledby="v-pills-profile-tab"
                      >
                        <form className="row">
                          <div className="col-md-12">
                            <label
                              htmlFor="validationCustom04"
                              className="form-label text-muted"
                            >
                              Country
                            </label>
                            <select className="form-select fw-normal py-2 text-dark">
                              <option value>India</option>
                              <option>...</option>
                            </select>
                          </div>
                          <div className="col-md-12 mt-5">
                            <label
                              htmlFor="validationCustom02"
                              className="form-label fw-normal text-muted"
                            >
                              City
                            </label>
                            <input
                              type="text"
                              className="form-control fw-normal py-2 text-dark"
                              placeholder="Gurgaon"
                            />
                          </div>
                          <div className="col-md-12 mt-5">
                            <button
                              className="fs-6 btn btn-primary w-100 rounded-pill py-2"
                              type="submit"
                            >
                              Search
                            </button>
                          </div>
                        </form>
                      </div>
                      <div
                        className="tab-pane fade"
                        id="v-pills-messages"
                        role="tabpanel"
                        aria-labelledby="v-pills-messages-tab"
                      >
                        <form className="row">
                          <div className="col-md-12">
                            <label
                              htmlFor="validationCustom04"
                              className="form-label text-muted"
                            >
                              Country
                            </label>
                            <select className="form-select fw-normal py-2 text-dark">
                              <option value>India</option>
                              <option>...</option>
                            </select>
                          </div>
                          <div className="col-md-12 mt-5">
                            <label
                              htmlFor="validationCustom02"
                              className="form-label fw-normal text-muted"
                            >
                              City
                            </label>
                            <input
                              type="text"
                              className="form-control fw-normal py-2 text-dark"
                              placeholder="Gurgaon"
                            />
                          </div>
                          <div className="col-md-12 mt-5">
                            <button
                              className="fs-6 btn btn-primary w-100 rounded-pill py-2"
                              type="submit"
                            >
                              Search
                            </button>
                          </div>
                        </form>
                      </div>
                      <div
                        className="tab-pane fade"
                        id="v-pills-settings"
                        role="tabpanel"
                        aria-labelledby="v-pills-settings-tab"
                      >
                        <form className="row">
                          <div className="col-md-12">
                            <label
                              htmlFor="validationCustom04"
                              className="form-label text-muted"
                            >
                              Country
                            </label>
                            <select className="form-select fw-normal py-2 text-dark">
                              <option value>India</option>
                              <option>...</option>
                            </select>
                          </div>
                          <div className="col-md-12 mt-5">
                            <label
                              htmlFor="validationCustom02"
                              className="form-label fw-normal text-muted"
                            >
                              City
                            </label>
                            <input
                              type="text"
                              className="form-control fw-normal py-2 text-dark"
                              placeholder="Gurgaon"
                            />
                          </div>
                          <div className="col-md-12 mt-5">
                            <button
                              className="fs-6 btn btn-primary w-100 rounded-pill py-2"
                              type="submit"
                            >
                              Search
                            </button>
                          </div>
                        </form>
                      </div>
                      <div
                        className="tab-pane fade"
                        id="packagetab"
                        role="tabpanel"
                        aria-labelledby="packagetab"
                      >
                        <form className="row">
                          <div className="col-md-12">
                            <label
                              htmlFor="validationCustom04"
                              className="form-label text-muted"
                            >
                              Country
                            </label>
                            <select className="form-select fw-normal py-2 text-dark">
                              <option value>India</option>
                              <option>...</option>
                            </select>
                          </div>
                          <div className="col-md-12 mt-5">
                            <label
                              htmlFor="validationCustom02"
                              className="form-label fw-normal text-muted"
                            >
                              City
                            </label>
                            <input
                              type="text"
                              className="form-control fw-normal py-2 text-dark"
                              placeholder="Gurgaon"
                            />
                          </div>
                          <div className="col-md-12 mt-5">
                            <button
                              className="fs-6 btn btn-primary w-100 rounded-pill py-2"
                              type="submit"
                            >
                              Search
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div className="dropdown">
                <button
                  className="border-0 bg-transparent"
                  type="button"
                  id="dropdownMenuButton1"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <img
                    src="/images/user_icon.png"
                    alt="user"
                    className="w-75"
                  />
                </button>
                <ul className="dropdown-menu dropdown-menu-end">
                  {isLogin.data.userType == "USER" && (
                    <>
                      <li>
                        <Link className="dropdown-item" to="/manage-profile">
                          Manage Account
                        </Link>
                      </li>
                      <li>
                        <Link className="dropdown-item" to="/student-profile">
                          My Profile
                        </Link>
                      </li>
                    </>
                  )}
                  {isLogin.data.userType == "CAMPUS" && (
                    <>
                      <li>
                        <Link className="dropdown-item" to="/campus-update">
                          Manage Account
                        </Link>
                      </li>
                      <li>
                        <Link className="dropdown-item" to="/campus-profile">
                          My Profile
                        </Link>
                      </li>
                    </>
                  )}
                  <li>
                    <a className="dropdown-item" onClick={userlogOut}>
                      Log out
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </nav>
      )}
      {!isLogin && (
        <nav className="navbar navbar-light py-3 px-5">
          <Link to="/" className="navbar-brand me-0">
            <img src="/images/logo.jpeg" alt="logo" className="w-75" />
          </Link>
          <form className="form-inline d-flex">
            <div className="dropdown">
              <button
                className="dropdown btn btn-common px-4 me-2 fw-light text-dark"
                id="navbarDropdown"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Register
                <span className="ms-2 iconLink">
                  <img src="/images/dropdown_icon.svg" alt="" />
                </span>
              </button>
              <ul
                className="dropdown-menu mt-3 iconLink"
                aria-labelledby="navbarDropdown"
              >
                <li>
                  <Link
                    className="dropdown-item fw-light px-3 py-2"
                    to="/student-register"
                  >
                    <span className="me-3">
                      <img src="/images/student_icon.svg" alt="student_icon" />
                    </span>
                    Student Register
                  </Link>
                </li>
                <li>
                  <Link
                    className="dropdown-item fw-light px-3 py-2"
                    to="/campus-register"
                  >
                    <span className="me-3">
                      <img src="/images/campus_icon.svg" alt="campus_icon" />
                    </span>
                    Register Your Campus
                  </Link>
                </li>
                <li>
                  <Link
                    className="dropdown-item fw-light px-3 py-2"
                    to="/company-register"
                  >
                    <span className="me-3">
                      <img src="/images/company_icon.svg" alt="company_icon" />
                    </span>
                    Company Registration
                  </Link>
                </li>
              </ul>
            </div>
            <button type="button" className="btn btn-common px-4">
              <Link to="/sign-in">Sign In</Link>
            </button>
          </form>
        </nav>
      )}
    </header>
  );
};

export default TopHead;
