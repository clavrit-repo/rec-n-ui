import { configureStore } from "@reduxjs/toolkit";
import userReducer from "../services/userSlice";

export default configureStore({
  reducer: {
    user: userReducer,
  },
});
