import axios from "axios";
import { useEffect, useState } from "react";
import authService, { API_URL } from "../../../services/auth-service";
import $ from "jquery";
import { flashMsg } from "../commonFunctions";

const Intrests = (props) => {
  //const user=authService.getCurrentUser();
  const [user, setUser] = useState(props.user);
  const [msg, setMsg] = useState("");
  const [userIntrestList, setIntrests] = useState(user.interests);

  useEffect(() => {
    console.log(userIntrestList);
    if (userIntrestList?.length) {
      window.$("input.intrest-tags").tagsinput();
      userIntrestList.split(",").forEach((intrest) => {
        window.$("input.intrest-tags").tagsinput("add", intrest);
      });
    } else {
      window.$("input.intrest-tags").tagsinput();
    }
  }, []);

  function updateItem() {
    debugger;
    let items = $("input.intrest-tags").val();
    let { id, firstName, lastName, emailId } = user;
    setMsg("Updating the Interests .....");
    axios
      .post(API_URL + `users/update`, {
        id,
        firstName,
        lastName,
        emailId,
        interests: items,
      })
      .then((response) => {
        if (response.data.code == 200) {
          setMsg("Interests Updates successfully !!");
        }
      })
      .finally(hideAlert);
  }
  const hideAlert = () => {
    window.setTimeout(() => {
      setMsg("");
    }, 4000);
  };

  return (
    <div id="row6" className="mb-5">
      <div className="card shadow border-0 py-3 px-2">
        <div className="card-body">
          {msg && flashMsg(msg)}
          <h5 className="text-dark fw-light mb-4">Interest</h5>
          <div className="border rounded p-1 my-4">
            <div className="tag-container">
              <input
                type="text"
                name="tags"
                className="form-control intrest-tags"
                data-role="tagsinput"
              />
              <button
                className="btn btn-primary"
                style={{ position: "absolute", right: "30px" }}
                onClick={updateItem}
              >
                Update
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Intrests;
