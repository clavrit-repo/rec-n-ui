import axios from "axios";
import { useEffect, useState } from "react";
import authService from "../../../services/auth-service";
import { API_URL } from "../../../services/auth-service";
import ActionNode from "../ActionNode";
import { soptions, statusClass, setLabel, flashMsg } from "../commonFunctions";

const ProjectNode = (props) => {
  const { id, onUpdate, onDelete } = props;
  const { name, score } = props.data;
  return (
    <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
      <div className="w-75">
        <h6>{name}</h6>
        <h6 className={statusClass(score)}>{setLabel(score)}</h6>
      </div>
      <ActionNode
        id={id}
        type="project"
        onDelete={onDelete}
        onUpdate={onUpdate}
      />
    </div>
  );
};

const Projects = (props) => {
  //const user=authService.getCurrentUser();
  const [user, setUser] = useState(props.user);
  const [msg, setMsg] = useState("");

  const [userProjects, setUserProjects] = useState(user.projects);
  const [name, setName] = useState("");
  const [status, setStatus] = useState("");
  const [isUpdate, setIsUpdate] = useState(false);
  const [isNotValid, setIsNotValid] = useState(false);

  useEffect(() => {
    getProjects();
  }, []);

  function getProjects() {
    axios.get(API_URL + `users/${user.id}/project`).then((response) => {
      if (response.status == 200 || response.data.code == 200) {
        setUserProjects(response.data.data);
      }
    });
  }

  function addProject({ name, score }) {
    if (!name || !score) {
      setIsNotValid(1);
      return;
    }
    axios
      .post(API_URL + `users/${user.id}/project`, {
        name,
        score,
        userId: user.id,
      })
      .then((response) => {
        if (response.status == 201 || response.data.code == 200) {
          //document.getElementById("Pform").reset();
          setMsg("Project Inserted Successfully !!!");
          setDefaults();
          getProjects();
        }
      })
      .catch((error) => {
        console.log(error.message);
      })
      .finally(hideAlert);
  }

  const deleteItem = (id) => {
    //axios.post(API_URL + `users/${user.id}/del_project/` + id + "/");
    axios
      .delete(API_URL + `project/` + id + "/")
      .then((response) => {
        if (response.status == 200 || response.data.code == 200) {
          setMsg("Project Delted successfully !!!");
          getProjects();
        }
      })
      .catch((error) => {
        console.log(error.message);
      })
      .finally(hideAlert);
  };

  function updateItem(item) {
    const { id, name, score } = item;
    setName(name);
    setStatus(score);
    setIsUpdate(id);
  }

  function setDefaults() {
    setName("");
    setStatus("");
    setIsUpdate(false);
    setIsNotValid(false);
  }

  const hideAlert = () => {
    window.setTimeout(() => {
      setMsg("");
    }, 4000);
  };

  function updateProject(id, item) {
    debugger;
    const nitem = { id, ...item, userId: user.id };
    //axios.post(API_URL + `users/${user.id}/project`, nitem);
    axios
      .put(API_URL + `project/${id}`, nitem)
      .then((response) => {
        if (response.status == 200 || response.data.code == 200) {
          setMsg("Project Updated Successfully !!!");
          setDefaults();
          getProjects();
        }
      })
      .catch((error) => {
        console.log(error.message);
      })
      .finally(hideAlert);
  }

  return (
    <div id="row4" className="mb-5">
      <div className="card shadow border-0 py-3 px-2">
        <div className="card-body">
          {msg && flashMsg(msg)}
          <div className="blockMob d-flex justify-content-between align-items-start">
            <div className="">
              <h5 className="text-dark fw-light mb-4">Projects</h5>
              <form id="Pform" className="row needs-validation" novalidate>
                <div className="col-md-12 mt-3">
                  <label
                    htmlFor="validationCustomUsername"
                    className="form-label fw-normal text-muted"
                  >
                    Projects Name
                  </label>
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control fw-normal text-sece py-2"
                      placeholder="Enter Name"
                      onChange={(e) => setName(e.target.value)}
                      value={name}
                    />
                  </div>
                  {!name && isNotValid && (
                    <span className="text-danger">This field is required</span>
                  )}
                </div>
                <div className="col-md-12 mt-5">
                  <label
                    htmlFor="validationCustomUsername"
                    className="form-label fw-normal text-muted"
                  >
                    Projects Status
                  </label>
                  <div className="input-group">
                    <select
                      className="form-select text-dark fw-normal py-2"
                      aria-label="Default select example"
                      onChange={(e) => setStatus(e.target.value)}
                      value={status}
                    >
                      <option value="">Select Status</option>
                      {soptions.map((i, idx) => (
                        <option key={idx} value={i.id}>
                          {i.text}
                        </option>
                      ))}
                    </select>
                  </div>
                  {!status && isNotValid && (
                    <span className="text-danger">This field is required</span>
                  )}

                  {!isUpdate && (
                    <button
                      className="btn btn-primary w-100 rounded-pill ms-1 py-2 mt-4 fs-5"
                      type="button"
                      onClick={() => addProject({ name, score: status })}
                    >
                      Add
                    </button>
                  )}

                  {isUpdate && (
                    <span>
                      <button
                        className="btn btn-primary w-20 rounded-pill ms-1 py-2 mt-4 fs-5"
                        type="button"
                        style={{ width: "30%" }}
                        onClick={() =>
                          updateProject(isUpdate, {
                            name,
                            score: status,
                          })
                        }
                      >
                        Update
                      </button>
                      <button
                        style={{ width: "40%" }}
                        className="btn btn-info w-30  rounded-pill ms-1 py-2 mt-4 fs-5"
                        type="button"
                        onClick={setDefaults}
                      >
                        Cancel Update
                      </button>
                    </span>
                  )}
                </div>
              </form>
            </div>
            <div className="w-50 rightblockMob">
              {userProjects &&
                userProjects.map((project, idx) => {
                  return (
                    <ProjectNode
                      data={project}
                      key={idx}
                      id={project.id}
                      onUpdate={() => updateItem(project)}
                      onDelete={deleteItem}
                    />
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Projects;
