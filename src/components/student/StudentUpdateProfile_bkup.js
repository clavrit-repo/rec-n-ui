import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import authService from "../../services/auth-service";
import HeadAfterLogin from "../includes/HeadAfterLogin";
import TopHead from "../includes/TopHead";


export const LeftNavBar = () => {
  return ( 
    <div className="col-md-3 mt-5 h-100 position-relative">
    <div
      className="down-arrow text-center pt-1 bg-secondary rounded-circle"
      data-toggle="ham-navigation"
    >
      <img
        src="images/right_arrow-icon.png"
        className="mm-2"
        alt="right_arrow"
      />
    </div>
    <div className="position-fixed infoNav h-100" id="ham-navigation">
      <nav className="border-0 card shadow text-dark">
        <ul className="menu p-0">
          <li
            className="border-bottom p-3 nav-btn active"
            data-row-id="row1"
          >
            <a href="#row1">Basic Information</a>
          </li>
          <li
            className="border-bottom p-3 nav-btn"
            data-row-id="row2"
          >
            <a href="#row2">Courses</a>
          </li>
          <li
            className="border-bottom p-3 nav-btn"
            data-row-id="row3"
          >
            <a href="#row3">Certification</a>
          </li>
          <li
            className="border-bottom p-3 nav-btn"
            data-row-id="row4"
          >
            <a href="#row4">Projects</a>
          </li>
          <li
            className="border-bottom p-3 nav-btn"
            data-row-id="row5"
          >
            <a href="#row5">Skills</a>
          </li>
          <li
            className="border-bottom p-3 nav-btn"
            data-row-id="row6"
          >
            <a href="#row6">Interest</a>
          </li>
          <li
            className="border-bottom p-3 nav-btn"
            data-row-id="row7"
          >
            <a href="#row7">Qualification</a>
          </li>
          <li className="border-0 p-3 nav-btn" data-row-id="row8">
            <a href="#row8">About</a>
          </li>
          <div id="nav-indicator"></div>
        </ul>
      </nav>
      <div className="text-center">
        <p className="text-muted mt-3">
          Member Since:{" "}
          <span className="text-dark">August 18, 2021</span>
        </p>
      </div>
      <button
        className="fs-5 btn btn-primary w-100 rounded-pill ms-1 py-2 mt-1 mx-2"
        type="submit"
      >
        Upload Resume
      </button>
    </div>
   </div>
   );
}

const UserInfo = (props) => {
 const  {user} = props;
  return ( 
    <div id="row1" className="mb-5 active">
                <div className="card shadow border-0 py-3 px-2">
                  <div className="card-body">
                    <h5 className="text-dark fw-light mb-4">
                      Basic Information
                    </h5>
                    <div className="basicInfo d-flex mx-4 mb-5">
                      <div className="w-25 me-5 text-center mt-5">
                        <figure className="m-0 me-3">
                          <img
                            src="https://picsum.photos/200"
                            alt=""
                            className="w-100 border border-5 rounded-circle"
                          />
                        </figure>
                        <button
                          type="button"
                          className="text-primary mt-5 bg-transparent border-0"
                        >
                          Replace Photo
                        </button>
                      </div>
                      <div className="w-75">
                        <form className="row needs-validation" novalidate>
                          <div className="col-md-6 position-relative mt-5">
                            <label
                              for="validationCustom01"
                              className="form-label fw-normal text-muted form-labelImp"
                            >
                             {user.firstName}
                            </label>
                            <div className="border-start border-danger border-2 position-absolute"></div>
                            <input
                              type="text"
                              className="form-control fw-normal py-2 text-dark"
                              id="validationCustom01"
                              placeholder="Sandeep"
                              required
                            />
                            <div className="valid-feedback">Looks good!</div>
                          </div>
                          <div className="col-md-6 position-relative mt-5">
                            <label
                              for="validationCustom01"
                              className="form-label fw-normal text-muted form-labelImp"
                            >
                               {user.lastName}
                            </label>
                            <div className="border-start border-danger border-2 position-absolute"></div>
                            <input
                              type="text"
                              className="form-control fw-normal py-2 text-dark"
                              id="validationCustom02"
                              placeholder="Singh"
                              required
                            />
                            <div className="valid-feedback">Looks good!</div>
                          </div>
                          <div className="col-md-6 position-relative mt-5">
                            <label
                              for="validationCustom04"
                              className="form-label form-labelImp text-muted"
                            >
                                {user.campus}
                            </label>
                            <div className="border-start border-danger border-2 position-absolute"></div>

                            <select
                              className="form-select fw-normal py-2 text-dark"
                              id="validationCustom04"
                              required
                            >
                              <option value="">IIT Delhi</option>
                              <option>...</option>
                            </select>
                            <div className="invalid-feedback">
                              Please select a valid state.
                            </div>
                          </div>
                          <div className="col-md-6 position-relative mt-5">
                            <label
                              for="validationCustom04"
                              className="form-label form-labelImp text-muted"
                            >
                              Country
                            </label>
                            <div className="border-start border-danger border-2 position-absolute"></div>

                            <select
                              className="form-select fw-normal py-2 text-dark"
                              id="validationCustom04"
                              required
                            >
                              <option value="">India</option>
                              <option>...</option>
                            </select>
                            <div className="invalid-feedback">
                              Please select a valid state.
                            </div>
                          </div>
                          <div className="col-md-6 mt-5">
                            <label
                              for="validationCustom02"
                              className="form-label fw-normal text-muted form-labelImp"
                            >
                              City
                            </label>
                            <input
                              type="text"
                              className="form-control fw-normal py-2 text-dark"
                              id="validationCustom02"
                              placeholder="Gurgaon"
                              required
                            />
                            <div className="valid-feedback">Looks good!</div>
                          </div>
                          <div className="col-md-6 mt-5">
                            <label
                              for="validationCustom02"
                              className="form-label fw-normal text-muted form-labelImp"
                            >
                              State
                            </label>
                            <input
                              type="text"
                              className="form-control fw-normal py-2 text-dark"
                              id="validationCustom02"
                              placeholder="Haryana"
                              required
                            />
                            <div className="valid-feedback">Looks good!</div>
                          </div>

                          <div className="col-md-6 mt-5">
                            <label
                              for="validationCustom02"
                              className="form-label fw-normal text-muted form-labelImp"
                            >
                              {" "}
                              Email Address
                            </label>
                            <input
                              type="email"
                              className="form-control fw-normal py-2 text-dark"
                              id="validationCustom02"
                              placeholder="abc@gmail.com"
                              required
                            />
                            <div className="valid-feedback">Looks good!</div>
                          </div>
                          <div className="col-md-6 mt-5">
                            <label
                              for="validationCustom02"
                              className="form-label fw-normal text-muted form-labelImp"
                            >
                              {" "}
                              Phone Number
                            </label>
                            <input
                              type="number"
                              className="form-control fw-normal py-2 text-dark"
                              id="validationCustom02"
                              placeholder="9222555566"
                              required
                            />
                            <div className="valid-feedback">Looks good!</div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
    </div>
   );
}
 
export {UserInfo};

export const CourseNode = (props) => {

 const {name,rate,rateTxt}=props.data;

  return (
    <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
    <div className="w-75">
      <h6>{name}</h6>
      <h6 className="text-warning">
        Course in Progress {rateTxt}{rate}
      </h6>
    </div>
    <div>
      <div className="dropdown me-3">
        <button
          className="click border-0 bg-transparent"
          data-row-id="dropdownMenuButton-1"
        >
          <img src="images/threeDots_img.png" alt="" />
        </button>
        <ul className="list p-0 dropdownMenuButton-1">
          <li>
            <a className="dropdown-item" href="#">
              Action
            </a>
          </li>
          <li>
            <a className="dropdown-item" href="#">
              Another action
            </a>
          </li>
          <li>
            <a className="dropdown-item" href="#">
              Something else here
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
    );
}
 



export const Courses = (props) => {
  //const {courses}=props;
  // defne the reactive state
  const { addCourse,deleteCourse,courses}=props;
  //const [courses,setCourses] =useState(props.courses);
  //alert(JSON.stringify(courses));

  const [courseName,setCourseName] = useState("");
  const [courseStatus,setCourseStatus] = useState("");

/*   function addCourse(){
    debugger;
    const newCourse = {
      id:courses.length+1,
      name:courseName,
      rate:courseStatus
    };
    courses.push(newCourse);
    setCourses(courses);
  }

  function deleteCourse(id){
    //let courses=[];
    let index= courses.some((course) => id===course.id )
    courses.splice(index,1);
    setCourses(courses);
  }
 */
if(!courses){
  return false
}else
  return ( 
    <div id="row2" className="mb-5">
    <div className="card shadow border-0 py-3 px-2">
      <div className="card-body">
        <div className="blockMob d-flex justify-content-between align-items-start">
          <div className="">
            <h5 className="text-dark fw-light mb-4">Courses</h5>
            <form className="row needs-validation" novalidate>
              <div className="col-md-12 mt-3">
                <label
                  for="validationCustomUsername"
                  className="form-label fw-normal text-muted"
                >
                  Course Name
                </label>
                <div className="input-group">
                  <input
                    type="text"
                    className="form-control fw-normal py-2 text-dark py-2"
                    placeholder="UI/UX Fundamental"
                    onChange={(e)=> setCourseName(e.target.value)}
                  />
                </div>
              </div>
              <div className="col-md-12 mt-5">
                <label
                  for="validationCustomUsername"
                  className="form-label fw-normal text-muted"
                >
                  Course Status
                </label>
                <div className="input-group">
                  <select
                    className="form-select text-dark fw-normal py-2"
                    aria-label="Default select example"
                    onChange={(e)=>{                  
                      setCourseStatus(e.target.value)}}
                  >
                    <option selected  value="1">10% to 40%</option>
                    <option value="2">41% to 60%</option>
                    <option value="3">61% to 80%</option>
                    <option value="4">81% to 100%</option>
                  </select>
                </div>
                <button
                  className="fs-5 btn btn-primary w-100 rounded-pill ms-1 py-2 mt-4"
                  type="button"
                  onClick={()=>addCourse({name:courseName,rate:courseStatus})}
                >
                  Add
                </button>
              </div>
            </form>
          </div>
          <div className="w-50 rightblockMob">
              {courses.length && [...courses].reverse().map((course)=><CourseNode data={course} />)}
            <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
              <div className="w-75">
                <h6>UI/UX Fundamental</h6>
                <h6 className="text-warning">
                  Course in Progress 10% to 40%
                </h6>
              </div>
              <div>
                <div className="dropdown me-3">
                  <button
                    className="click border-0 bg-transparent"
                    data-row-id="dropdownMenuButton-1"
                  >
                    <img src="images/threeDots_img.png" alt="" />
                  </button>

                  <ul className="list p-0 dropdownMenuButton-1">
                    <li>
                      <a className="dropdown-item" href="#">
                        Action
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="#">
                        Another action
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="#">
                        Something else here
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
              <div className="w-75">
                <h6>UI/UX Fundamental</h6>
                <h6 className="text-info">
                  Course in Progress 40% to 90%
                </h6>
              </div>
              <div>
                <div className="dropdown me-3">
                  <button
                    className="click border-0 bg-transparent"
                    data-row-id="dropdownMenuButton-2"
                  >
                    <img src="images/threeDots_img.png" alt="" />
                  </button>

                  <ul className="list p-0 dropdownMenuButton-2">
                    <li>
                      <a className="dropdown-item" href="#">
                        Action
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="#">
                        Another action
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="#">
                        Something else here
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
              <div className="w-75">
                <h6>UI/UX Fundamental</h6>
                <h6 className="text-success opacity-75">
                  Course Completed 100%
                </h6>
              </div>
              <div>
                <div className="dropdown me-3">
                  <button
                    className="click border-0 bg-transparent"
                    data-row-id="dropdownMenuButton-3"
                  >
                    <img src="images/threeDots_img.png" alt="" />
                  </button>

                  <ul className="list p-0 dropdownMenuButton-3">
                    <li>
                      <a className="dropdown-item" href="#">
                        Action
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="#">
                        Another action
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="#">
                        Something else here
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
   );
}

const Skills = () => {
  return ( 
    <div id="row5" className="mb-5">
    <div className="card shadow border-0 py-3 px-2">
      <div className="card-body">
        <h5 className="text-dark fw-light mb-4">Skills</h5>
        <div className="border rounded p-1 my-4">
          <div className="tag-container">
            <input
              type="text"
              name="tags"
              className="form-control"
              data-role="tagsinput"
            />
          </div>
        </div>
      </div>
    </div>
  </div>
   );
}
export {Skills};

const QualificationNode = (props) => {

   const {streams}=props; 
   if(streams){
         return  streams.map((stream)=>{
            return ( 
                <div className="col-md-6 mb-3">
                  <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
                    <div className="w-75">
                      <h6>{stream.name}</h6>
                      <h6 className="text-warning">{stream.score+"%"}</h6>
                    </div>
                    <div>
                      <div className="dropdown me-3">
                        <button
                          className="click border-0 bg-transparent"
                          data-row-id="dropdownMenuButton-10"
                        >
                          <img src="images/threeDots_img.png" alt="" />
                        </button>
                        <ul className="list p-0 dropdownMenuButton-10">
                          <li>
                            <a className="dropdown-item" href="#">
                              Action
                            </a>
                          </li>
                          <li>
                            <a className="dropdown-item" href="#">
                              Another action
                            </a>
                          </li>
                          <li>
                            <a className="dropdown-item" href="#">
                              Something else here
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
               );  
           })
   }    
}
 
export {QualificationNode};


const Qualification = () => {
  const streams=['Class 10th','Class 12th','Graduate','BA','Master','MBA','MCA'];
  const userStreams=[
    {
        name:"Class 10th",
        score:"45.5"
    },
    {
        name:"MBA",
        score:"85.5"
    }
  ];

  return (
    <div id="row7" className="mb-5">
    <div className="card shadow border-0 py-3 px-2">
      <div className="card-body">
        <div className="blockMob d-flex justify-content-between align-items-start">
          <div className="">
            <h5 className="text-dark fw-light mb-4">
              Qualification
            </h5>
            <form className="row needs-validation" novalidate>
              <div className="col-md-12 mt-3">
                <label
                  for="validationCustomUsername"
                  className="form-label fw-normal text-muted"
                >
                  School or University
                </label>
                <div className="input-group">
                  <select
                    className="form-select text-dark fw-normal py-2"
                    aria-label="Default select example"
                  >
                    {streams.map((item,idx)=> <option key={idx} >{i}</option>)}
                    <option value="1">Class 12th</option>
                  </select>
                </div>
              </div>
              <div className="col-md-12 mt-5">
                <label
                  for="validationCustomUsername"
                  className="form-label fw-normal text-muted"
                >
                  Precentage of Marks
                </label>
                <div className="input-group">
                  <input
                    type="text"
                    className="form-control fw-normal text-sece py-2"
                    placeholder="72.5%"
                  />
                </div>
                <button
                  className="btn btn-primary w-100 rounded-pill ms-1 py-2 mt-4 fs-5"
                  type="submit"
                >
                  Add
                </button>
              </div>
            </form>
          </div>
          <div className="w-50 row rightblockMob">
            <QualificationNode streams={userStreams} />

            <div className="col-md-6 mb-3">
              <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
                <div className="w-75">
                  <h6>Class 10th</h6>
                  <h6 className="text-warning">72.5%</h6>
                </div>
                <div>
                  <div className="dropdown me-3">
                    <button
                      className="click border-0 bg-transparent"
                      data-row-id="dropdownMenuButton-10"
                    >
                      <img src="images/threeDots_img.png" alt="" />
                    </button>

                    <ul className="list p-0 dropdownMenuButton-10">
                      <li>
                        <a className="dropdown-item" href="#">
                          Action
                        </a>
                      </li>
                      <li>
                        <a className="dropdown-item" href="#">
                          Another action
                        </a>
                      </li>
                      <li>
                        <a className="dropdown-item" href="#">
                          Something else here
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6 mb-3">
              <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
                <div className="w-75">
                  <h6>Class 12th</h6>
                  <h6 className="text-warning">65.5%</h6>
                </div>
                <div>
                  <div className="dropdown me-3">
                    <button
                      className="click border-0 bg-transparent"
                      data-row-id="dropdownMenuButton-11"
                    >
                      <img src="images/threeDots_img.png" alt="" />
                    </button>

                    <ul className="list p-0 dropdownMenuButton-11">
                      <li>
                        <a className="dropdown-item" href="#">
                          Action
                        </a>
                      </li>
                      <li>
                        <a className="dropdown-item" href="#">
                          Another action
                        </a>
                      </li>
                      <li>
                        <a className="dropdown-item" href="#">
                          Something else here
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6 mb-3">
              <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
                <div className="w-75">
                  <h6>B.A</h6>
                  <h6 className="text-warning">77.5%</h6>
                </div>
                <div>
                  <div className="dropdown me-3">
                    <button
                      className="click border-0 bg-transparent"
                      data-row-id="dropdownMenuButton-12"
                    >
                      <img src="images/threeDots_img.png" alt="" />
                    </button>

                    <ul className="list p-0 dropdownMenuButton-12">
                      <li>
                        <a className="dropdown-item" href="#">
                          Action
                        </a>
                      </li>
                      <li>
                        <a className="dropdown-item" href="#">
                          Another action
                        </a>
                      </li>
                      <li>
                        <a className="dropdown-item" href="#">
                          Something else here
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6 mb-3">
              <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
                <div className="w-75">
                  <h6>Graduate</h6>
                  <h6 className="text-warning">72.5%</h6>
                </div>
                <div>
                  <div className="dropdown me-3">
                    <button
                      className="click border-0 bg-transparent"
                      data-row-id="dropdownMenuButton-13"
                    >
                      <img src="images/threeDots_img.png" alt="" />
                    </button>

                    <ul className="list p-0 dropdownMenuButton-13">
                      <li>
                        <a className="dropdown-item" href="#">
                          Action
                        </a>
                      </li>
                      <li>
                        <a className="dropdown-item" href="#">
                          Another action
                        </a>
                      </li>
                      <li>
                        <a className="dropdown-item" href="#">
                          Something else here
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    );
}
 
export {Qualification};


const Intrests = () => {
  return (
    <div id="row6" className="mb-5">
    <div className="card shadow border-0 py-3 px-2">
      <div className="card-body">
        <h5 className="text-dark fw-light mb-4">Interest</h5>
        <div className="border rounded p-1 my-4">
          <div className="tag-container">
            <input
              type="text"
              name="tags"
              className="form-control"
              data-role="tagsinput"
            />
          </div>
        </div>
      </div>
    </div>
  </div>
    );
}
 
export {Intrests};

 
const About = () => {
  return (  
  <div id="row8" className="mb-5">
  <div className="card shadow border-0 py-3 px-2">
    <div className="card-body">
      <h5 className="text-dark fw-light">About</h5>
      <div className="mt-4 border p-2 rounded" id="menu1 ">
        <p className="text-muted">
          Lorem, ipsum dolor sit amet consectetur adipisicing
          elit. Laboriosam sint animi voluptatem deserunt rem
          illum accusamus, nostrum iste porro praesentium vel
          neque non consectetur tempora laborum ea reprehenderit
          adipisci fugit? Lorem, ipsum dolor sit amet consectetur
          adipisicing elit. Laboriosam sint animi voluptatem
          deserunt rem illum accusamus, nostrum iste porro
          praesentium vel neque non consectetur tempora laborum ea
          reprehenderit adipisci fugit?
        </p>
      </div>
    </div>
  </div>
  </div> );
} 
export {About} ;

 
const StudentUpdateProfile = (props) => {
  
  const {loggedIn}=props;
  const navigate=useNavigate();

  const [user,setUser] = useState(()=>{
         return authService.getCurrentUser();
  })

  const [courses,setCourses] =useState(user.courses);

  console.log("user status:",loggedIn,user);

  useEffect(()=>{
    if(user==null){
      navigate("/");
    }
  },[]);


  function addCourse({name:courseName,rate:courseStatus}){
    debugger;
    const newCourse = {
      id:courses.length+1,
      name:courseName,
      rate:courseStatus
    };
    //let tt=[...courses];
    setCourses([...courses,newCourse]);


    /* courses.push(newCourse); // direct updation 
    setCourses(()=>{
      alert('statat');
      alert(courses);
      return courses
    }); */


  /*   setUser((ps)=>{
      return {...ps,courses:newarr}}  
    ); */
  }

  function deleteCourse(id){
    //let courses=[];
    let index= courses.some((course) => id===course.id )
    courses.splice(index,1);
    setCourses(courses);
  }




  if(!user)
  return false
  else
  return (
    <React.Fragment>
      <section className="profileViewSec py-5">
        <div className="container">
          <div className="row profileView">
            <LeftNavBar/>
            <div className="col-md-9 mt-5 h-100">
              <UserInfo user={user}   />

              {courses &&  <Courses user={user} courses={courses} addCourse={addCourse} deleteCourse={deleteCourse} />}  
             
          
              <div id="row3" className="mb-5">
                <div className="card shadow border-0 py-3 px-2">
                  <div className="card-body">
                    <div className="blockMob d-flex justify-content-between align-items-start">
                      <div className="">
                        <h5 className="text-dark fw-light mb-4">
                          Certification
                        </h5>
                        <form className="row needs-validation" novalidate>
                          <div className="col-md-12 mt-3">
                            <label
                              for="validationCustomUsername"
                              className="form-label fw-normal text-muted"
                            >
                              {" "}
                              Certification Name
                            </label>
                            <div className="input-group">
                              <input
                                type="text"
                                className="form-control fw-normal text-sece py-2"
                                placeholder="Enter Name"
                              />
                            </div>
                          </div>
                          <div className="col-md-12 mt-5">
                            <label
                              for="validationCustomUsername"
                              className="form-label fw-normal text-muted"
                            >
                              {" "}
                              Certification Status
                            </label>
                            <div className="input-group">
                              <select
                                className="form-select text-dark fw-normal py-2"
                                aria-label="Default select example"
                              >
                                <option>Select Status</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                              </select>
                            </div>
                            <button
                              className="btn btn-primary w-100 rounded-pill ms-1 py-2 mt-4 fs-5"
                              type="submit"
                            >
                              Add
                            </button>
                          </div>
                        </form>
                      </div>
                      <div className="w-50 rightblockMob">
                        <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
                          <div className="w-75">
                            <h6>UI/UX Fundamental</h6>
                            <h6 className="text-warning">
                              Course in Progress 10% to 40%
                            </h6>
                          </div>
                          <div>
                            <div className="dropdown me-3">
                              <button
                                className="click border-0 bg-transparent"
                                data-row-id="dropdownMenuButton-4"
                              >
                                <img src="images/threeDots_img.png" alt="" />
                              </button>

                              <ul className="list p-0 dropdownMenuButton-4">
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Action
                                  </a>
                                </li>
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Another action
                                  </a>
                                </li>
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Something else here
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
                          <div className="w-75">
                            <h6>UI/UX Fundamental</h6>
                            <h6 className="text-info">
                              Course in Progress 40% to 90%
                            </h6>
                          </div>
                          <div>
                            <div className="dropdown me-3">
                              <button
                                className="click border-0 bg-transparent"
                                type="button"
                                data-row-id="dropdownMenuButton-5"
                              >
                                <img src="images/threeDots_img.png" alt="" />
                              </button>
                              <ul className="list p-0 dropdownMenuButton-5">
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Action
                                  </a>
                                </li>
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Another action
                                  </a>
                                </li>
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Something else here
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
                          <div className="w-75">
                            <h6>UI/UX Fundamental</h6>
                            <h6 className="text-success opacity-75">
                              Course Completed 100%
                            </h6>
                          </div>
                          <div>
                            <div className="dropdown me-3">
                              <button
                                className="click border-0 bg-transparent"
                                data-row-id="dropdownMenuButton-6"
                              >
                                <img src="images/threeDots_img.png" alt="" />
                              </button>

                              <ul className="list p-0 dropdownMenuButton-6">
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Action
                                  </a>
                                </li>
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Another action
                                  </a>
                                </li>
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Something else here
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="row4" className="mb-5">
                <div className="card shadow border-0 py-3 px-2">
                  <div className="card-body">
                    <div className="blockMob d-flex justify-content-between align-items-start">
                      <div className="">
                        <h5 className="text-dark fw-light mb-4">Projects</h5>
                        <form className="row needs-validation" novalidate>
                          <div className="col-md-12 mt-3">
                            <label
                              for="validationCustomUsername"
                              className="form-label fw-normal text-muted"
                            >
                              {" "}
                              Projects Name
                            </label>
                            <div className="input-group">
                              <input
                                type="text"
                                className="form-control fw-normal text-sece py-2"
                                placeholder="Enter Name"
                              />
                            </div>
                          </div>
                          <div className="col-md-12 mt-5">
                            <label
                              for="validationCustomUsername"
                              className="form-label fw-normal text-muted"
                            >
                              {" "}
                              Projects Status
                            </label>
                            <div className="input-group">
                              <select
                                className="form-select text-dark fw-normal py-2"
                                aria-label="Default select example"
                              >
                                <option>Select Status</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                              </select>
                            </div>
                            <button
                              className="btn btn-primary w-100 rounded-pill ms-1 py-2 mt-4 fs-5"
                              type="submit"
                            >
                              Add
                            </button>
                          </div>
                        </form>
                      </div>
                      <div className="w-50 rightblockMob">
                        <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
                          <div className="w-75">
                            <h6>Gather Client Feedback Guide</h6>
                            <h6 className="text-warning">
                              Project in Progress 10% to 40%
                            </h6>
                          </div>
                          <div>
                            <div className="dropdown me-3">
                              <button
                                className="click border-0 bg-transparent"
                                data-row-id="dropdownMenuButton-7"
                              >
                                <img src="images/threeDots_img.png" alt="" />
                              </button>

                              <ul className="list p-0 dropdownMenuButton-7">
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Action
                                  </a>
                                </li>
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Another action
                                  </a>
                                </li>
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Something else here
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
                          <div className="w-75">
                            <h6>Fundamentals of Design Thinking</h6>
                            <h6 className="text-info">
                              Project in Progress 40% to 90%
                            </h6>
                          </div>
                          <div>
                            <div className="dropdown me-3">
                              <button
                                className="click border-0 bg-transparent"
                                data-row-id="dropdownMenuButton-8"
                              >
                                <img src="images/threeDots_img.png" alt="" />
                              </button>

                              <ul className="list p-0 dropdownMenuButton-8">
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Action
                                  </a>
                                </li>
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Another action
                                  </a>
                                </li>
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Something else here
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
                          <div className="w-75">
                            <h6>Team Workflows in JIRA Platform</h6>
                            <h6 className="text-success opacity-75">
                              Project Completed 100%
                            </h6>
                          </div>
                          <div>
                            <div className="dropdown me-3">
                              <button
                                className="click border-0 bg-transparent"
                                data-row-id="dropdownMenuButton-9"
                              >
                                <img src="images/threeDots_img.png" alt="" />
                              </button>

                              <ul className="list p-0 dropdownMenuButton-9">
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Action
                                  </a>
                                </li>
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Another action
                                  </a>
                                </li>
                                <li>
                                  <a className="dropdown-item" href="#">
                                    Something else here
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                <Skills/>
                <Intrests/>
                <Qualification/>
            
              <About/>
             
            </div>
          </div>
        </div>
      </section>
     {/*  </main> */}
    </React.Fragment>
  );
};

export default StudentUpdateProfile;
