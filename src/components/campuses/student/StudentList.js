const StudentList = (props) => {
  const { students } = props;
  if (!students) return false;
  else
    return (
      <div className="col-xl-3 col-lg-12 col-md-12 mt-5">
        <div className="card shadow border-0 py-3 h-100">
          <div className="card-body">
            <div className="cardhead">
              <div className="d-flex justify-content-between position-relative">
                <h5 className="w-50 fs-5 fw-light">Students List</h5>
                <span className="w-50 justify-content-end d-flex">
                  <img
                    src="images/expand.svg"
                    className="listIcon"
                    alt="expand"
                  />
                  <img
                    src="images/search.svg"
                    className="listIcon mx-2 click"
                    data-row-id="dropdownMenuButton-12"
                    alt="search"
                  />
                  <img
                    src="images/filter.svg"
                    className="listIcon"
                    alt="filter"
                  />
                </span>
                <div className="w-100 mt-5 p-3 border-0 card position-absolute shadow list dropdownMenuButton-12">
                  <input
                    type="text"
                    placeholder="search..."
                    className="form-control bg-none"
                  />
                </div>
              </div>
            </div>
            <div className="cardbody mt-5">
              <table className="table studentTable">
                <thead className="table-light text-muted">
                  <tr>
                    <th className="border-0" />
                    <th className="border-0 py-3 fw-light pl-3">
                      Student Name
                    </th>
                    <th className="border-0 py-3 fw-light">Stream</th>
                    <th className="border-0 py-3 fw-light">Rank</th>
                  </tr>
                </thead>
                <tbody className="text-muted fw-light">
                  {students &&
                    students.map((student) => {
                      return (
                        <tr>
                          <td className="align-bottom py-3">
                            <span>
                              <img className src="images/message.png" />
                            </span>
                          </td>
                          <td className="py-3">
                            <span>
                              <img
                                className="listIcon me-2"
                                src="images/user.svg"
                              />
                            </span>
                            {student?.firstName}
                          </td>
                          <td className="py-3"> {student.stream}</td>
                          <td className="py-3">{student?.ugcRank}</td>
                        </tr>
                      );
                    })}
                </tbody>
              </table>
              <nav aria-label="...">
                <ul className="pagination justify-content-end">
                  <li className="page-item active">
                    <a className="page-link" href="#">
                      1
                    </a>
                  </li>
                  <li className="page-item" aria-current="page">
                    <a className="page-link" href="#">
                      2
                    </a>
                  </li>
                  <li className="page-item">
                    <a className="page-link" href="#">
                      3
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    );
};

export default StudentList;
