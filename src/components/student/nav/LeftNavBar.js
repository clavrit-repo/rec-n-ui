const  LeftNavBar = () => {
    return (
      <div className="col-md-3 mt-5 h-100 position-relative">
        <div
          className="down-arrow text-center pt-1 bg-secondary rounded-circle"
          data-toggle="ham-navigation"
        >
          <img
            src="images/right_arrow-icon.png"
            className="mm-2"
            alt="right_arrow"
          />
        </div>
        <div className="position-fixed infoNav h-100" id="ham-navigation">
          <nav className="border-0 card shadow text-dark">
            <ul className="menu p-0">
              <li className="border-bottom p-3 nav-btn active" data-row-id="row1">
                <a href="#row1">Basic Information</a>
              </li>
              <li className="border-bottom p-3 nav-btn" data-row-id="row2">
                <a href="#row2">Courses</a>
              </li>
              <li className="border-bottom p-3 nav-btn" data-row-id="row3">
                <a href="#row3">Certification</a>
              </li>
              <li className="border-bottom p-3 nav-btn" data-row-id="row4">
                <a href="#row4">Projects</a>
              </li>
              <li className="border-bottom p-3 nav-btn" data-row-id="row5">
                <a href="#row5">Skills</a>
              </li>
              <li className="border-bottom p-3 nav-btn" data-row-id="row6">
                <a href="#row6">Interest</a>
              </li>
              <li className="border-bottom p-3 nav-btn" data-row-id="row7">
                <a href="#row7">Qualification</a>
              </li>
              <li className="border-0 p-3 nav-btn" data-row-id="row8">
                <a href="#row8">About</a>
              </li>
              <div id="nav-indicator"></div>
            </ul>
          </nav>
          <div className="text-center">
            <p className="text-muted mt-3">
              Member Since: <span className="text-dark">August 18, 2021</span>
            </p>
          </div>
          <button
            className="fs-5 btn btn-primary w-100 rounded-pill ms-1 py-2 mt-1 mx-2"
            type="button"
          >
            Upload Resume
          </button>
        </div>
      </div>
    );
  };
  export default LeftNavBar;