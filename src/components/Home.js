import React, { useEffect, useState } from "react";
import HomeBudge from "./includes/HomeBudge";
import HeadBeforeLogin from "./includes/HeadBeforLogin";
import HeadAfterLogin from "./includes/HeadAfterLogin";
import authService from "../services/auth-service";
import TopHead from "./includes/TopHead";

const Home = (props) => {

  //const {loggedIn}=props;

  const [loggedIn, setLoggedIn] =useState(false)
  useEffect(()=>{
    if(authService.getCurrentUser()){
      setLoggedIn(true);
    }
    },[]); 

  /*  function logoutHome(){
    alert('logout at home called');
    authService.logout();
    setLoggedIn(false);
   } 
    console.log("**************** Home :",loggedIn); */


    return (
        <React.Fragment>
          {/* <main> */}

           {/*     {loggedIn && <HeadAfterLogin onLogout={logoutHome}/> }
               {!loggedIn && <HeadBeforeLogin/> } */}

           {/*  <TopHead loggedIn={loggedIn} page="home"/>    */} 

            <section className="mainSection vh-100 position-relative py-5">
              <div className="container profileView py-5">
                <div className="row align-items-center my-4 welBlock">
                  {!loggedIn && <div className="col-md-12 mx-auto">
                    <form action="" className="d-flex justify-content-center">
                      <div className="bg-white input-group mb-4 border rounded-pill p-2 w-50 searchInput">
                        <div className="input-group-prepend border-0">
                          <button
                            id="button-addon4"
                            type="button"
                            className="btn btn-link text-muted"
                          >
                            <img src="images/search_icon.png" alt="" />
                          </button>
                        </div>
                        <input
                          type="search"
                          placeholder="Search Campus, Student, Institution"
                          aria-describedby="button-addon4"
                          className="fw-bold form-control bg-none border-0 mr-1"
                        />
                      </div>
                    </form>
                  </div>}
                  <div className="col-md-12 mx-auto text-center mt-4">
                    <h1 className="fw-lighter text-muted">
                      Welcome To Your <br />
                      Professional Community
                    </h1>
                  </div>
                </div>
                <div className="row align-items-center ">
                  <div className="col-lg-12 col-md-12 registerBlocks mt-4 mb-3">
                    <HomeBudge
                      title="Campus Registered"
                      count="25"
                      icon="images/campus_Img.svg"
                    />
                  </div>
                  <div className="col-lg-6 col-md-12 registerBlocks my-3">
                    <HomeBudge
                      title="Student Registered"
                      count="10"
                      icon="images/student_Img.svg"
                    />
                  </div>
                  <div className="col-lg-6 col-md-12 registerBlocks my-3">
                    <HomeBudge
                      title="Company Registered"
                      count="5"
                      icon="images/company_Img.svg"
                    />
                  </div>
                </div>
              </div>
            </section>

        {/*   </main> */}
        </React.Fragment>
      );
}
 
export default Home;