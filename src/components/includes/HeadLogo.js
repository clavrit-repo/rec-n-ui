import { Link } from "react-router-dom";
const HeadLogo = () => {
  return (
    <header>
      <nav className="navbar navbar-light py-3  px-5">
        <Link to="/" className="navbar-brand">
          <img src="images/logo.jpeg" alt="logo" className="w-75" />
        </Link>
      </nav>
    </header>
  );
};
export default HeadLogo;
