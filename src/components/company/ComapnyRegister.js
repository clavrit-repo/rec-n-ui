import HeadAfterLogin from "../includes/HeadAfterLogin";
import TopHead from "../includes/TopHead";
import { appendErrors, useForm } from "react-hook-form";
import * as yup from "yup";
import BottomWidgets from "../includes/BottomWidgets";
import { yupResolver } from "@hookform/resolvers/yup";
import authService from "../../services/auth-service";
import { useNavigate } from "react-router-dom";
import { useState } from "react";

const CompanyRegister = (props) => {
  const [resMsg, setResmsg] = useState("");

  const validationSchema = yup.object().shape({
    firstName: yup.string().required(),
    lastName: yup.string().required(),
    company: yup.string().required(),
    jobTitle: yup.string().required(),
    email: yup.string().email().required(),
    phone: yup.number().required(),
    password: yup
      .string()
      .required("Password is required")
      .min("Password must be at least 6 characters"),
    cpassword: yup
      .string()
      .required()
      .oneOf([yup.ref("password")], "Password must match"),
  });
  //const validateForm=validatinSchema.is
  const formOptions = { resolver: yupResolver(validationSchema) };

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm(formOptions);

  const navigate = useNavigate();

  const onSubmit = (data) => {
    alert(JSON.stringify(data, null, 2));

    const {
      campusName,
      campusEmail: emailId,
      password,
      phone,
      ugcRank,
      studentCount: studentNo,
      companyCount: companyVisited,
    } = data;

    authService
      .registerCampus({
        campusName,
        emailId,
        password,
        phone,
        ugcRank,
        studentNo,
        companyVisited,
      })
      .then(function (response) {
        //alert(response.data);
        if (response.status === 200) {
          authService
            .login(emailId, password)
            .then((user) => {
              if (user.data.userType == "USER") {
                navigate("/student-profile");
              }
              if (user.data.userType == "CAMPUS") {
                navigate("/campus-profile");
              }
              if (user.data.userType == "COMPANY") {
                navigate("/company-profile");
              }
            })
            .catch((error) => {
              setResmsg(error.message);
            });
          setResmsg(
            "Campus Registered successfully!.Pls Login to update Profile"
          );
          //document.getElementById("formCampus").reset();
        }
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  return (
    <main>
      <section className="mainSection vh-100">
        <div className="container">
          <div className="row justify-content-center align-items-center mx-1 logInForm h-auto">
            <div className="col-lg-5 col-md-7 shadow p-5 bg-white mt-5">
              <h3 className="fw-normal">
                {" "}
                <span className="me-3 iconLink">
                  <img src="images/company_icon.svg" alt />
                </span>
                Company Registration
              </h3>
              <form
                className="row needs-validation"
                onSubmit={handleSubmit(onSubmit)}
              >
                <div className="col-md-6 position-relative mt-5">
                  <label
                    htmlFor="validationCustom01"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    First name
                  </label>
                  <div className="border-start border-danger border-2 position-absolute" />
                  <input
                    type="text"
                    className="form-control fw-bold text-dark"
                    id="validationCustom01"
                    placeholder="Sandeep"
                    name="firstName"
                    {...register("firstName", { required: true })}
                  />
                  <div className="invalid-feedback d-block">
                    {errors.firstName?.message}
                  </div>
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted"
                  >
                    Last name
                  </label>
                  <input
                    type="text"
                    className="form-control fw-bold text-dark"
                    id="validationCustom02"
                    placeholder="Singh"
                    name="lastName"
                    {...register("lastName", { required: true })}
                  />
                  <div className="invalid-feedback d-block">
                    {errors.lastName?.message}
                  </div>
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal
                    text-muted form-labelImp"
                  >
                    Company Name
                  </label>
                  <input
                    type="text"
                    className="form-control fw-bold text-dark"
                    id="validationCustom02"
                    placeholder="ABC pvt Ltd."
                    name="company"
                    {...register("company", { required: true })}
                  />
                  <div className="invalid-feedback d-block">
                    {errors.company?.message}
                  </div>
                </div>
                <div className="col-md-6 mt-5">
                  <label htmlFor="" className="form-label fw-normal text-muted">
                    Job Title
                  </label>
                  <input
                    type="text"
                    className="form-control fw-bold text-dark"
                    id=""
                    placeholder="Creative Head"
                    name="jobTitle"
                    {...register("jobTitle", { required: true })}
                  />
                  <div
                    className={
                      "invalid-feedback " + (errors.jobTitle ? "d-block" : "")
                    }
                  >
                    {errors.company?.message}
                  </div>
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor=""
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Work Email Address
                  </label>
                  <input
                    type="email"
                    className="form-control fw-bold text-dark"
                    id=""
                    placeholder="abc@gmail.com"
                    name="email"
                    {...register("email", { required: true })}
                  />
                  <div className="invalid-feedback d-block">
                    {errors.email?.message}
                  </div>
                </div>
                <div className="col-md-6 mt-5">
                  <label htmlFor="" className="form-label fw-normal text-muted">
                    Work Phone Number
                  </label>
                  <input
                    type="text"
                    className="form-control fw-bold text-dark"
                    id=""
                    placeholder={9222555566}
                    name="phone"
                    {...register("phone", { required: true })}
                  />
                  <div className="invalid-feedback d-block">
                    {errors.phone?.message}
                  </div>
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Password
                  </label>
                  <input
                    type="password"
                    className="form-control fw-normal py-2 text-dark"
                    placeholder="password"
                    name="password"
                    {...register("password", { required: true })}
                  />
                  <div
                    className={`invalid-feedback ${
                      errors.password ? " d-block" : ""
                    }`}
                  >
                    {errors.password?.message}
                  </div>
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Confirm Password
                  </label>
                  <input
                    type="password"
                    className="form-control fw-normal py-2 text-dark"
                    placeholder="Confirm your password"
                    name="cpassword"
                    {...register("cpassword", { required: true })}
                  />
                  <div
                    className={`invalid-feedback ${
                      errors.cpassword ? " d-block" : ""
                    }`}
                  >
                    {errors.cpassword?.message}
                  </div>
                </div>

                <div className="col-12 d-flex mt-5">
                  <button
                    className="btn btn-outline-secondary w-50 rounded-pill me-1"
                    type="button"
                  >
                    Cancel
                  </button>
                  <button
                    className="btn btn-primary w-50 rounded-pill ms-1"
                    type="submit"
                  >
                    Register
                  </button>
                </div>
              </form>
            </div>
          </div>

          <BottomWidgets />
        </div>
      </section>
    </main>
  );
};

export default CompanyRegister;
