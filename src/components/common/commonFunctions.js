export const studentStreams = [
  "Class 10th",
  "Class 12th",
  "Graduate",
  "Below Bachelor Degree",
  "BA",
  "Master",
  "MBA",
  "MCA",
  "Post Graduate Diploma",
];

export const soptions = [
  { id: 1, text: "below 10%" },
  { id: 2, text: "10% to 40%" },
  { id: 3, text: "40% to 80%" },
  { id: 4, text: "100%" },
];

export function statusClass(status) {
  status = parseInt(status);
  switch (status) {
    case 1:
      return "text-warning";
      break;
    case 2:
      return "text-warning";
      break;
    case 3:
      return "text-info";
      break;
    case 4:
      return "text-success";
      break;
    default:
      return "text-warning";
      break;
  }
}

export function sClass(status) {
  status = parseInt(status);
  switch (status) {
    case 1:
      return "warning";
      break;
    case 2:
      return "warning";
      break;
    case 3:
      return "info";
      break;
    case 4:
      return "success";
      break;
    default:
      return "warning";
      break;
  }
}
export function sPercentage(status) {
  status = parseInt(status);
  switch (status) {
    case 1:
      return 10;
      break;
    case 2:
      return 40;
      break;
    case 3:
      return 80;
      break;
    case 4:
      return 100;
      break;
    default:
      return 0;
      break;
  }
}

export const setLabel = function (rate) {
  switch (parseInt(rate)) {
    case 1:
      return <span>Course in Progress 10%</span>;
      break;
    case 2:
      return "Course in Progress 10% to 40%";
      break;
    case 3:
      return "Course in Progress 40% to 80%";
      break;
    case 4:
      return "Course Completed 100%";
      break;
    default:
      return "";
      break;
  }
};

export function flashMsg(msg, type = "success") {
  debugger;
  if (type == "success")
    return (
      <div className="text-danger pb-4 text-center">
        <strong>{msg}</strong>
      </div>
    );
  if (type == "warning")
    return <div className="alert alert-warning">{msg}</div>;
  if (type == "danger") return <div className="alert alert-danger">{msg}</div>;
  return <div className="alert alert-info">{msg}</div>;
}
