import React, { Component } from "react";

class ClassComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoding: null,
      error: null,
      data: [],
    };
  }

  componentDidMount() {
  /*   fetch('https://jsonplaceholder.typicode.com/posts/1')
        .then((response) => {
            console.log(response)
           return response.json();  
        })
        .then((json) => console.log(json))
        .catch(error=>{
            alert('error occured');
        }); */

    fetch("https://jsonplaceholder.typicode.com/todos/?_limit=10")
    .then(response => response.json())
    .then( data => {   
        //console.log(data)
        this.setState({data:data});
        console.log("state set",this.state.data);
    })
    .catch(error => console.log(error));
  }

  render() {
    return (
      <div className="container">
        <h1>it is class component</h1>
         {this.state.data.map((item)=>{
             return (
                 <li>{item.title} <b>Status</b> {item.completed ? "yes":"NO"}</li>
             )
         })} 
      </div>
    );
  }
}

export default ClassComponent;
