import { useState } from "react";

const AddJob = (props) => {
  const { addJob } = props;
  const [jobtitle, setJobTitle] = useState("");
  const [experience, setExperience] = useState("");
  const [skills, setSkills] = useState("");
  const [expected_package, setPackage] = useState("");
  const [invalid, setInvalid] = useState(false);
  const [msg, setMsg] = useState(false);

  const createJob = function (item) {
    //debugger;
    if (!jobtitle || !experience || !skills || !expected_package) {
      console.log("validation Error");
      setInvalid(true);
      setMsg("");
    } else {
      addJob(item).then(() => {
        setJobTitle("");
        setExperience("");
        setSkills("");
        setPackage("");
        setInvalid(false);
        setMsg("New Job Created successfully!");
      });
    }
  };

  return (
    <div
      className="modal fade"
      id="job-add-model"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabIndex={-1}
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content rounded-0">
          <div className="modal-header border-0">
            <h5 className="modal-title" id="staticBackdropLabel">
              Add New Opening
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            />
          </div>
          <div className="modal-body px-4">
            <form className="row">
              {msg && <p className="text-danger text-center">{msg}</p>}
              <div className="col-md-6 position-relative mt-lg-2 mt-5 ">
                <label
                  htmlFor="validationCustom01"
                  className="form-label fw-normal text-muted"
                >
                  Job Title
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  id="validationCustom01"
                  placeholder=""
                  value={jobtitle}
                  onChange={(e) => setJobTitle(e.target.value)}
                />
                {invalid && !jobtitle && (
                  <p className="text-danger">This field is required</p>
                )}
              </div>
              <div className="col-md-6 mt-lg-2 mt-5">
                <label
                  htmlFor="validationCustom02"
                  className="form-label fw-normal text-muted"
                >
                  Experience Required
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  id="validationCustom02"
                  placeholder=""
                  value={experience}
                  onChange={(e) => setExperience(e.target.value)}
                />
                {invalid && !experience && (
                  <p className="text-danger">This field is required</p>
                )}
              </div>
              <div className="col-md-6 mt-5">
                <label
                  htmlFor="validationCustom02"
                  className="form-label fw-normal text-muted"
                >
                  Expected offered package
                </label>
                <input
                  type="email"
                  className="form-control fw-bold text-dark"
                  placeholder=""
                  value={expected_package}
                  onChange={(e) => setPackage(e.target.value)}
                />
                {invalid && !expected_package && (
                  <p className="text-danger">This field is required</p>
                )}
              </div>
              <div className="col-md-6 mt-5">
                <label
                  htmlFor="validationCustom02"
                  className="form-label fw-normal text-muted"
                >
                  Expected skills
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  placeholder=""
                  value={skills}
                  onChange={(e) => setSkills(e.target.value)}
                />
                {invalid && !skills && (
                  <p className="text-danger">This field is required</p>
                )}
              </div>
              <div className="col-12 d-flex my-5">
                <button
                  className="btn btn-outline-secondary w-50 rounded-pill me-1"
                  data-bs-dismiss="modal"
                  type="button"
                >
                  Cancel
                </button>
                <button
                  className="btn btn-primary w-50 rounded-pill ms-1"
                  type="button"
                  /*  data-bs-dismiss="modal" */
                  onClick={() =>
                    createJob({
                      jobtitle,
                      experience,
                      skills,
                      expected_package,
                    })
                  }
                >
                  Register
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
export default AddJob;
