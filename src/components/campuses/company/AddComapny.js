import { useForm } from "react-hook-form";

import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import { useState } from "react";
import { flashMsg } from "../commonFunctions";

const AddCompany = () => {
  const [msg, setMsg] = useState();

  const vSchema = yup.object().shape({
    companyName: yup.string().required(),
    companyEmail: yup.string().email("Required valid email ID").required(),
    companyPhone: yup.string().required(),
  });
  const validateForm = { resolver: yupResolver(vSchema) };

  const onSubmit = function (data) {
    //alert(JSON.stringify(data));
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm(validateForm);

  return (
    <div id="row4" className="mb-5">
      <div className="card shadow border-0 py-3 px-2">
        <div className="card-body">
          {msg && flashMsg(msg)}
          <div className="blockMob d-flex justify-content-between align-items-start">
            <div className="">
              <h5 className="text-dark fw-light mb-4">Add New Company</h5>
              <form
                id="Pform"
                onSubmit={handleSubmit(onSubmit)}
                className="row needs-validation"
                novalidate
              >
                <div className="col-md-4 mt-3">
                  <label
                    htmlFor="validationCustomUsername"
                    className="form-label fw-normal text-muted"
                  >
                    Company Name
                  </label>
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control fw-normal text-sece py-2"
                      placeholder="Enter Name"
                      name="companyName"
                      {...register("companyName", { required: true })}
                    />
                  </div>
                  {errors.companyName && (
                    <span className="text-danger">This field is required</span>
                  )}
                </div>
                <div className="col-md-4 mt-3">
                  <label
                    htmlFor="validationCustomUsername"
                    className="form-label fw-normal text-muted"
                  >
                    Contact Email
                  </label>
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control fw-normal text-sece py-2"
                      placeholder="Enter contact Email"
                      name="companyEmail"
                      {...register("companyEmail", { required: true })}
                    />
                  </div>
                  {errors.companyEmail && (
                    <span className="text-danger">
                      {errors.companyEmail.message}
                    </span>
                  )}
                </div>
                <div className="col-md-4 mt-3">
                  <label
                    htmlFor="validationCustomUsername"
                    className="form-label fw-normal text-muted"
                  >
                    Contact Phone
                  </label>
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control fw-normal text-sece py-2"
                      placeholder="Enter contact Phone"
                      name="companyPhone"
                      {...register("companyPhone", { required: true })}
                    />
                  </div>
                  {errors.companyPhone && (
                    <span className="text-danger">This field is required</span>
                  )}
                </div>
                <div className="col-md-3 mt-5">
                  <button
                    className="btn btn-primary w-100 rounded-pill ms-1 py-2 mt-4 fs-5"
                    type="submit"
                  >
                    Add
                  </button>
                </div>
              </form>
            </div>
            {/*  <div className="w-50 rightblockMob">
                  {userProjects && userProjects.map((project,idx) =>{
                      return  <ProjectNode data={project}  key={idx} id={idx} onUpdate={updateItem} onDelete={deleteItem}  />
                  })}    
              </div> */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddCompany;
