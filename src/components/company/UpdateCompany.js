import axios from "axios";
import { useState } from "react";
import { API_URL } from "../../services/auth-service";

const UpdateCompany = (props) => {
  const { company, onUpdate } = props;
  const [firstName, setFirstName] = useState(() => company?.firstName);
  const [lastName, setLastName] = useState(company?.lastName);
  const [emailId, setEmail] = useState(company?.emailId);
  const [phone, setPhone] = useState(company?.phone);
  const [companyName, setCompanyName] = useState(company?.company);
  const [designation, setDesignation] = useState(company?.designation);
  const [invalid, setInvalid] = useState(false);
  const [msg, setMsg] = useState(false);

  const handleSubmit = function (id) {
    if (!firstName || !lastName || !emailId || !phone) {
      setInvalid(true);
      if (msg) setMsg("");
    } else {
      onUpdate(id, {
        firstName,
        lastName,
        emailId,
        phone,
        designation,
        company: companyName,
      }).then(() => {
        setMsg("Company Profile Updated successfully !!!");
      });

      /*  UpdateCompany(id, {
        firstName,
        lastName,
        emailId,
        phone,
        designation,
        company: companyName,
      }); */
    }
  };

  /* function UpdateCompany(id, data) {
    axios.put(API_URL + "companies/" + id, data).then((response) => {
      console.log("Company Updated========", response.data);
      setMsg("Company Profile Updated successfully !!!");
      //setEditDone(true);
      return response.data;
    });
  } */

  return (
    <div
      className="modal fade"
      id="edit-details"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabIndex={-1}
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content rounded-0">
          <div className="modal-header border-0">
            <h5 className="modal-title" id="staticBackdropLabel">
              Edit Details
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            />
          </div>
          <div className="modal-body px-4">
            {msg && <p className="text-danger text-center">{msg}</p>}
            <form className="row">
              <div className="col-md-6 position-relative mt-lg-2 mt-5 ">
                <label
                  htmlFor="validationCustom01"
                  className="form-label fw-normal text-muted"
                >
                  First name
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  id="validationCustom01"
                  placeholder="Sandeep"
                  name="firstName"
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                />
                {invalid && !firstName && (
                  <p className="text-danger">This field is required</p>
                )}
              </div>
              <div className="col-md-6 mt-lg-2 mt-5">
                <label
                  htmlFor="validationCustom02"
                  className="form-label fw-normal text-muted"
                >
                  Last name
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  id="validationCustom02"
                  placeholder="Singh"
                  name="lastName"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                />
                {invalid && !lastName && (
                  <p className="text-danger">This field is required</p>
                )}
              </div>

              <div className="col-md-6 mt-5">
                <label
                  htmlFor="validationCustom02"
                  className="form-label fw-normal text-muted"
                >
                  Company name
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  id="validationCustom02"
                  placeholder="Singh"
                  name="companyName"
                  value={companyName}
                  onChange={(e) => setCompanyName(e.target.value)}
                />
                {invalid && !companyName && (
                  <p className="text-danger">This field is required</p>
                )}
              </div>
              <div className="col-md-6  mt-5">
                <label
                  htmlFor="validationCustom02"
                  className="form-label fw-normal text-muted"
                >
                  Comapny Designation
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  id="validationCustom02"
                  placeholder="Singh"
                  name="designation"
                  value={designation}
                  onChange={(e) => setDesignation(e.target.value)}
                />
                {invalid && !designation && (
                  <p className="text-danger">This field is required</p>
                )}
              </div>

              <div className="col-md-6 mt-5">
                <label
                  htmlFor="validationCustom02"
                  className="form-label fw-normal text-muted"
                >
                  Email Address
                </label>
                <input
                  type="email"
                  className="form-control fw-bold text-dark"
                  placeholder="sandeep.singh@ask9.com"
                  name="emailId"
                  value={emailId}
                  onChange={(e) => setEmail(e.target.value)}
                />
                {invalid && !emailId && (
                  <p className="text-danger">This field is required</p>
                )}
              </div>
              <div className="col-md-6 mt-5">
                <label
                  htmlFor="validationCustom02"
                  className="form-label fw-normal text-muted"
                >
                  Phone Number
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  placeholder="0214 4054090"
                  name="phone"
                  value={phone}
                  onChange={(e) => setPhone(e.target.value)}
                />
                {invalid && !phone && (
                  <p className="text-danger">This field is required</p>
                )}
              </div>
              <div className="col-12 d-flex my-5">
                <button
                  className="btn btn-outline-secondary w-50 rounded-pill me-1"
                  data-bs-dismiss="modal"
                  type="button"
                >
                  Cancel
                </button>
                <button
                  className="btn btn-primary w-50 rounded-pill ms-1"
                  type="button"
                  /* data-bs-dismiss="modal" */
                  onClick={() => handleSubmit(2)}
                >
                  Register
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UpdateCompany;
