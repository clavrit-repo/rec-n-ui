import { Link } from "react-router-dom";

function ProfileView(props) {
  debugger;
  const { campus } = props;
  return (
    <div className="card shadow border-0 py-3 px-2 h-100">
      <div className="card-body">
        <div className="cardhead">
          <div>
            <h4 className="textblue">{campus.campusName}</h4>
          </div>
          <figure className="m-0 mt-3 me-3">
            <img src="images/institute.svg" alt className="w-100" />
          </figure>
        </div>
        <div className="d-flex mt-3 mb-5">
          <figure className="m-0 me-3 w-25">
            <img src="images/ugc_icon.svg" alt className />
          </figure>
          <div className="mt-3">
            <h6 className="text-muted fw-light">UGC Rank</h6>
            <h4 className="textblue">{campus.ugcRank}</h4>
          </div>
        </div>
        <div className="cardbody mt-5">
          <div className="mt-5 mb-2">
            <h5 className="text-muted fw-light m-0">{campus.campusName}</h5>
            <a className="text-primary fw-light">{campus.emailId}</a>
          </div>
          <div className>
            <h6 className="text-muted fw-light">0214 4054090</h6>
            <h6 className="text-muted">Gurgaon,Haryana,India</h6>
          </div>
          <div className="d-flex my-5">
            <button
              className="btn btn-primary py-2 w-100 rounded-pill ms-1 btn-sm"
              type="submit"
            >
              <Link to="/campus-update" className="text-white">
                Edit Details
              </Link>
            </button>
          </div>
        </div>
      </div>
      <div className="card-footer text-center bg-white border-0 text-muted text-small">
        <p>
          Member Since:
          <span className="text-dark">August 18, 2021</span>
        </p>
      </div>
    </div>
  );
}
export default ProfileView;
