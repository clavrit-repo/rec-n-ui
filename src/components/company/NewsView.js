export const NewsView = () => {
  return (
    <div className="col-xl-12 col-lg-12 col-md-12 mt-4">
      <div className="card shadow border-0 px-2 h-100">
        <div className="card-body">
          <h4 className="fs-5 fw-light">News</h4>
          <div
            id="carouselExampleControls"
            className="carousel slide mt-3"
            data-bs-ride="carousel"
          >
            <div className="carousel-inner">
              <div className="carousel-item active">
                <div className="row">
                  <div className="col d-flex align-items-center">
                    <img
                      src="images/news_image.svg"
                      alt="news_image"
                      className="w-25"
                    />
                    <div className="ms-2">
                      <h6>
                        Internet trolls take pleasure in making you suffer
                      </h6>
                      <p className="m-0">
                        <span className="textblue">GADGET</span> | 1 HOUR AGO
                      </p>
                    </div>
                  </div>
                  <div className="col d-flex align-items-center">
                    <img
                      src="images/news_image.svg"
                      alt="news_image"
                      className="w-25"
                    />
                    <div className="ms-2">
                      <h6>
                        Internet trolls take pleasure in making you suffer
                      </h6>
                      <p className="m-0">
                        <span className="textblue">GADGET</span> | 1 HOUR AGO
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="carousel-item">
                <div className="row">
                  <div className="col d-flex align-items-center">
                    <img
                      src="images/news_image.svg"
                      alt="news_image"
                      className="w-25"
                    />
                    <div className="ms-2">
                      <h6>
                        Internet trolls take pleasure in making you suffer
                      </h6>
                      <p className="m-0">
                        <span className="textblue">GADGET</span> | 1 HOUR AGO
                      </p>
                    </div>
                  </div>
                  <div className="col d-flex align-items-center">
                    <img
                      src="images/news_image.svg"
                      alt="news_image"
                      className="w-25"
                    />
                    <div className="ms-2">
                      <h6>
                        Internet trolls take pleasure in making you suffer
                      </h6>
                      <p className="m-0">
                        <span className="textblue">GADGET</span> | 1 HOUR AGO
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="carousel-item">
                <div className="row">
                  <div className="col d-flex align-items-center">
                    <img
                      src="images/news_image.svg"
                      alt="news_image"
                      className="w-25"
                    />
                    <div className="ms-2">
                      <h6>
                        Internet trolls take pleasure in making you suffer
                      </h6>
                      <p className="m-0">
                        <span className="textblue">GADGET</span> | 1 HOUR AGO
                      </p>
                    </div>
                  </div>
                  <div className="col d-flex align-items-center">
                    <img
                      src="images/news_image.svg"
                      alt="news_image"
                      className="w-25"
                    />
                    <div className="ms-2">
                      <h6>
                        Internet trolls take pleasure in making you suffer
                      </h6>
                      <p className="m-0">
                        <span className="textblue">GADGET</span> | 1 HOUR AGO
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <button
              className="carousel-control-prev carouselbtn"
              type="button"
              data-bs-target="#carouselExampleControls"
              data-bs-slide="prev"
            >
              <span className="carousel-control-prev-icon" aria-hidden="true" />
              <span className="visually-hidden">Previous</span>
            </button>
            <button
              className="carousel-control-next carouselbtn carouselnext"
              type="button"
              data-bs-target="#carouselExampleControls"
              data-bs-slide="next"
            >
              <span className="carousel-control-next-icon" aria-hidden="true" />
              <span className="visually-hidden">Next</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

const AboutView = () => {
  return (
    <div className="col-xl-12 col-lg-12 col-md-12 mt-4">
      <div className="card shadow border-0 px-2 h-100">
        <div className="card-body">
          <h4 className="fs-5 fw-light">About</h4>
          <div className="mt-3" id="menu1">
            <ul className="term-list p-0">
              <li className="term-item">
                <p className="text-muted">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Laboriosam sint animi voluptatem deserunt rem illum accusamus,
                  nostrum iste porro praesentium vel neque non consectetur
                  tempora laborum ea reprehenderit adipisci fugit?
                </p>
              </li>
              <li className="term-item">
                <p className="moretext text-muted">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Laboriosam sint animi voluptatem deserunt rem illum accusamus,
                  nostrum iste porro praesentium vel neque non consectetur
                  tempora laborum ea reprehenderit adipisci fugit?
                </p>
              </li>
              <li className="term-item">
                <a className="moreless-button justify-content-end text-muted">
                  ...see more
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
export { AboutView };
