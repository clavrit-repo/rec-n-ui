import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import authService from "../../services/auth-service";

const HeadAfterLogin = (props) => {

  console.log("HeadAfter=======", props);

/* function onLogout(){
  alert('onlogut event triggre');
} */

  return (
    <header>
      <nav className="navbar navbar-expand-lg navbar-light px-5 py-3 bg-white w-100 shadow-sm">
        <div className="container-fluid justify-content-between">
          <div>
            <Link to="/" className="navbar-brand me-0">
              <img src="images/logo.jpeg" alt="logo" className="w-75" />
            </Link>
          </div>
          <div>
            <div className="dropdown">
              <button
                className="border-0 bg-transparent"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <img src="images/user_icon.png" alt="user" className="w-75" />
              </button>
              <ul className="dropdown-menu dropdown-menu-end">
                  <li>
                    <Link className="dropdown-item" to="/manage-profile">
                     Manage Account
                    </Link>
                  </li>
                  <li>
                    <Link className="dropdown-item" to="/student-profile">
                     My Profile 
                    </Link>
                  </li>
                  <li>
                    <a className="dropdown-item" onClick={props.onLogout}>
                    Log out
                    </a>
                  </li>
                </ul>
            </div>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default HeadAfterLogin;
