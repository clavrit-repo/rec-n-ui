const ActionNode = (props) => {
  const { id, type, onDelete, onUpdate } = props;
  return (
    <div>
      <div className="dropdown me-3">
        <button
          className="click border-0 bg-transparent"
          data-row-id={type + "-" + id}
        >
          <img src="images/threeDots_img.png" alt="" />
        </button>
        <ul className={`list p-0 ${type}-` + id}>
          <li>
            <a
              className="dropdown-item"
              href="#"
              onClick={(e) => {
                e.preventDefault();
                props.onUpdate(id);
              }}
            >
              EDIT
            </a>
          </li>
          <li>
            <a
              className="dropdown-item"
              href="#"
              onClick={(e) => {
                e.preventDefault();
                props.onDelete(id);
              }}
            >
              Delete
            </a>
          </li>
          <li>
            <a className="dropdown-item" href="#">
              Something else here
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};
export default ActionNode;
