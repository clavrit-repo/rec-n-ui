import { useDispatch } from "react-redux";
import { userx } from "../services/userSlice";

import { useForm } from "react-hook-form";
import HeadLogo from "./includes/HeadLogo";
import authService from "../services/auth-service";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import TopHead from "./includes/TopHead";

const StudentLogin = () => {
  const dispatch = useDispatch();
  const [resMsg, setResmsg] = useState("Please login "); //define the rectve state

  const navigate = useNavigate();
  console.log("user is :", authService.loggedIn);

  useEffect(() => {
    if (authService.getCurrentUser()) {
      //navigate("/student-profile");
    }
  }, []);

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();
  console.log("form error", errors);

  function onSubmit(data) {
    //alert(JSON.stringify(data));
    authService
      .login(data.email, data.password)

      .then((user) => {
        dispatch(userx());
        switch (user.data.userType) {
          case "STUDENT":
            navigate("/student-profile");
            break;
          case "COMPANY":
            navigate("/company-profile");
            break;
          case "CAMPUS":
            navigate("/campus-profile");
            break;
          case "USER":
            navigate("/student-profile");
            break;
          default:
            //navigate("/");
            navigate("/student-profile");
            break;
        }
      })
      .catch((error) => {
        debugger;
        const msg =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        setResmsg(msg);
      });
  }

  return (
    <>
      <section className="mainSection vh-100">
        <div className="container">
          {/*  <HeadLogo /> */}
          <div className="row justify-content-center align-items-center mx-1 logInForm h-auto">
            <div className="col-lg-5 col-md-7 shadow p-5 bg-white ">
              {resMsg && (
                <div className="alert alert-success" role="alert">
                  {resMsg}
                </div>
              )}

              <h3 className="fw-normal mb-5">
                <span className="me-3 iconLink">
                  <img src="images/signin_icon.svg" alt="" />
                </span>
                Sign In
              </h3>

              <form
                className="row needs-validation"
                onSubmit={handleSubmit(onSubmit)}
                noValidate
              >
                <div className="col-md-12">
                  <label
                    htmlFor="validationCustomUsername"
                    className="form-label fw-normal text-muted"
                  >
                    User Name/Email Address
                  </label>
                  <div className="input-group has-validation">
                    <input
                      type="text"
                      className="form-control  fw-bold text-dark"
                      placeholder="sandeep.singh@ask9.com"
                      id="validationCustomUsername"
                      aria-describedby="inputGroupPrepend"
                      required
                      name="email"
                      {...register("email", { required: true, value: "" })}
                    />
                    <div
                      className="invalid-feedback"
                      style={{ display: errors.email ? "block" : "" }}
                    >
                      Please choose a username.
                    </div>
                  </div>
                </div>
                <div className="col-md-12 mt-5">
                  <label
                    htmlFor="validationCustom03"
                    className="form-label fw-normal text-muted"
                  >
                    Password
                  </label>
                  <div className="input-group position-relative">
                    <input
                      name="password"
                      type="password"
                      placeholder="***************"
                      className="form-control fw-bold"
                      id="validationCustom03"
                      required
                      {...register("password", { required: true, value: "" })}
                    />
                    <div
                      className="invalid-feedback"
                      style={{ display: errors.password ? "block" : "" }}
                    >
                      Please valid Password.
                    </div>
                    <span className="position-absolute end-0 me-4 pw-Icon">
                      <img src="images/pw-icon.png" alt="" />
                    </span>
                  </div>
                </div>
                <div className="col-md-12 my-5">
                  <div className="d-flex justify-content-between align-items-center">
                    <div className="form-check">
                      <input
                        className="form-check-input rounded-0"
                        type="checkbox"
                        id="invalidCheck"
                        required
                        name="rememberMe"
                        checked
                        {...register("rememberMe")}
                      />
                      <label
                        className="form-check-label"
                        htmlFor="invalidCheck"
                      >
                        Keep Me Logged In
                      </label>
                    </div>
                    <div className="invalid-feedback">
                      You must agree before submitting.
                    </div>
                    <h6 className="float-end text-end sm-small"></h6>
                  </div>
                </div>
                <div className="col-12">
                  <button
                    className="btn btn-primary w-100 rounded-pill "
                    type="submit"
                  >
                    Log In
                  </button>
                </div>
              </form>
            </div>
          </div>
          {/* <!-- Login Form end  --> */}
          <div className="row mt-5 justify-content-center">
            <div className="col-xl-3 col-lg-4 col-md-6">
              <div className="registerBlocks opacity-75">
                <ul className="w-100 d-flex justify-content-between align-items-center p-4 shadow bg-white mx-auto">
                  <li className="w-25">
                    <img src="images/campus_Img.svg" alt="" />
                  </li>
                  <li className="w-75 d-flex justify-content-end">
                    <h2 className="font-weight-normal">25</h2>
                    <h5 className="text-muted fw-light ms-4">
                      {" "}
                      Campus
                      <br />
                      Registered
                    </h5>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xl-3 col-lg-4 col-md-6 opacity-75">
              <div className="registerBlocks">
                <ul className="w-100 d-flex justify-content-between align-items-center p-4 shadow bg-white mx-auto">
                  <li className="w-25">
                    <img src="images/student_Img.svg" alt="" />
                  </li>
                  <li className="w-75 d-flex justify-content-end">
                    <h2 className="font-weight-normal">5K</h2>
                    <h5 className="text-muted fw-light ms-4">
                      {" "}
                      Student
                      <br />
                      Registered
                    </h5>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xl-3 col-lg-4 col-md-6 opacity-75">
              <div className="registerBlocks">
                <ul className="w-100 d-flex justify-content-between align-items-center p-4 shadow bg-white mx-auto">
                  <li className="w-25">
                    <img src="images/company_Img.svg" alt="" />
                  </li>
                  <li className="w-75 d-flex justify-content-end">
                    <h2 className="font-weight-normal">10</h2>
                    <h5 className="text-muted fw-light ms-4">
                      {" "}
                      Company
                      <br />
                      Registered
                    </h5>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default StudentLogin;
