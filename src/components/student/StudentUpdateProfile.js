import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import authService from "../../services/auth-service";
import HeadAfterLogin from "../includes/HeadAfterLogin";
import TopHead from "../includes/TopHead";
import UserInfo from "./user/UserInfo";
import Courses from "./course/Courses";
import Projects from "./project/Projects";
import Skills from "./skill/Skills";
import Intrests from "./intrests/Intrests";
import Qualification from "./qualification/Qualification";
import Certificates from "./certificate/Certificates";
import LeftNavBar from "./nav/LeftNavBar";
import About from "./news/About";

const StudentUpdateProfile = (props) => {
  //const { loggedIn } = props;
  const navigate = useNavigate();

  const [isLogin, setLoggedIn] = useState(() => {
    return authService.getLogin();
  });

  const [user, setUser] = useState(null);
  const [courses, setCourses] = useState(null);

  //console.log("user status:", loggedIn, user);

  useEffect(() => {
    if (isLogin == null) {
      navigate("/");
    } else {
      authService.getCurrentUser().then((user) => {
        setUser(user);
        console.log("******************** STU PROFILR", user);
      });
    }
  }, []);

  function addCourse({ name: courseName, rate: courseStatus }) {
    debugger;
    const newCourse = {
      id: courses.length + 1,
      name: courseName,
      rate: courseStatus,
    };
    setCourses([...courses, newCourse]);
    document.getElementById("Pform").reset();
  }

  function deleteCourse(id) {
    //let courses=[];
    let index = courses.some((course) => id === course.id);
    courses.splice(index, 1);
    setCourses(courses);
  }

  if (!user) return false;
  else
    return (
      <React.Fragment>
        <section className="profileViewSec py-5">
          <div className="container">
            <div className="row profileView">
              <LeftNavBar />
              <div className="col-md-9 mt-5 h-100">
                <UserInfo user={user} />
                {courses && (
                  <Courses
                    user={user}
                    courses={courses}
                    addCourse={addCourse}
                    deleteCourse={deleteCourse}
                  />
                )}
                <Courses user={user} />
                <Certificates user={user} />
                <Projects user={user} />
                <Skills user={user} />
                <Intrests user={user} />
                <Qualification user={user} />
                <About />
              </div>
            </div>
          </div>
        </section>
        {/*  </main> */}
      </React.Fragment>
    );
};

export default StudentUpdateProfile;
