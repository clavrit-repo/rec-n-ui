
  function putDummyData(data){
    data.password="12345";
    data.courses=[
      {
        "name":"Mechanical Engineering",
        "rate":7
      },
     {
        "name":"Electrical Engineering",
        "rate":4
      },
      {
        "name":"Civil Engineering",
        "rate":9
      },
      {
        "name":"Chemical Engineering",
        "rate":6
      },{
        "name":"Mechanical Engineering",
        "rate":7
      },{
        "name":"Biochemical Engineering",
        "rate":5
      },
      {
        "name":"Aerospace Engineering",
        "rate":5
      }
    ];
    data.projects=[
      {
        "name":"TATA Steel IOT Implementation",
        "rate":7
      },
      {
        "name":"PMO Smart city Managemnet Dev Production",
        "rate":4
      },
      {
        "name":"User Graghics AI Lens Application",
        "rate":9
      },
      {
        "name":"Game zone IOT Implementation",
        "rate":6
      }
    ];
    data.skills=["Angular 2.0","React 17.0","REact NAtive","PHP","JAVA","Android"];
    data.intruests=["Managemnet","Photoshop","IOT","Python","go language"];
    return data;
  }
