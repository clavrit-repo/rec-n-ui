const JobView = (props) => {
  const { openings, onDelete, onUpdate } = props;
  return (
    <div className="card shadow border-0 py-3 h-100">
      <div className="card-body">
        <div className="cardhead">
          <div className="d-flex justify-content-between position-relative">
            <h5 className="fs-5 fw-light">Current Job Openings</h5>
            <span className="w-25 justify-content-end d-flex">
              <img
                src="/images/search.svg"
                className="plus_icon click"
                data-row-id="dropdownMenuButton-10"
                alt
              />
              {/*  <img src="images/plus_icon.svg" className="plus_icon mx-1" alt /> */}
              <button
                className="border-0 bg-transparent"
                data-bs-toggle="modal"
                data-bs-target="#job-add-model"
              >
                <img
                  src="/images/plus_icon.svg"
                  className="mx-2 plus_icon"
                  alt="plus_icon"
                />
              </button>
              <img src="/images/filter.svg" className="plus_icon" alt />
            </span>
            <div className="w-100 mt-5 shadow p-3 border-0 card position-absolute list dropdownMenuButton-10">
              <input
                type="text"
                placeholder="search..."
                className="form-control bg-none"
              />
            </div>
          </div>
        </div>
        <div className="cardbody mt-3">
          <div className="row">
            {openings && (
              <JobCard
                openings={openings}
                onDelete={onDelete}
                onUpdate={onUpdate}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default JobView;

const JobCard = function ({ openings, onDelete, onUpdate }) {
  //debugger;
  //console.log(onDelete);
  return openings?.map((job) => {
    return (
      <div className="col-xl-12 col-lg-6" key={job.id}>
        <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
          <div className="w-75">
            <h6>{job.jobtitle}</h6>
            <h6 className="text-warning m-0">{job.experience}</h6>
          </div>
          <div>
            <div className="dropdown me-3">
              <button
                className="click border-0 bg-transparent"
                /*  data-row-id="dropdownMenuButton-1" */
                data-row-id={"jobmenu-" + job.id}
              >
                <img src="/images/threeDots_img.png" alt />
              </button>
              <ul
                /*  className="list p-0 dropdownMenuButton-1" */
                className={`list p-0 jobmenu-${+job.id}`}
              >
                <li>
                  <a
                    className="dropdown-item"
                    data-bs-toggle="modal"
                    data-bs-target="#job-update-modal"
                    onClick={(e) => {
                      e.preventDefault();
                      onUpdate(job);
                    }}
                    href="#"
                  >
                    Edit
                  </a>
                </li>
                <li>
                  <a
                    className="dropdown-item"
                    href="#"
                    onClick={(e) => {
                      e.preventDefault();
                      onDelete(job.id);
                    }}
                  >
                    Delete
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  });
};
