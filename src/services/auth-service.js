import axios from "axios";

// Add a response interceptor

// axios.interceptors.response.use(function (response) {
//     // Any status code that lie within the range of 2xx cause this function to trigger
//     // Do something with response data
//     debugger
//    /*  if(response.data.length==1){
//         response.data=response.data[0];
//         response.data.accessToken="jfkshdfihew233453534";
//     } */
//     if(response.data){
//       response.data.accessToken="jfkshdfihew233453534";
//     }
//     return response;
//   }, function (error) {
//     // Any status codes that falls outside the range of 2xx cause this function to trigger
//     // Do something with response error
//     return Promise.reject(error);
//   });

//const API_URL = "http://localhost:8080/api/auth/";
//const API_URL = "http://localhost:3004/";
//export const API_URL = "http://4268-103-167-115-121.ngrok.io/";
//export const API_URL = "http://8429-103-167-115-124.ngrok.io/";
//export const API_URL = "http://localhost:3004/";
export const API_URL = "http://34.126.219.95:8080/";

const ISMOCK = 0;

const axiosConfig = {
  headers: {
    "Content-Type": "application/json;charset=UTF-8",
    "Access-Control-Allow-Origin": "*",
  },
};

export const soptions = [
  { id: 1, text: "below 10%" },
  { id: 2, text: "10% to 40%" },
  { id: 3, text: "40% to 80%" },
  { id: 4, text: "100%" },
];

class AuthService {
  loggedIn = false;
  redirectUrl = "";
  login1(username, password) {
    debugger;
    return axios
      .post(API_URL, {
        username,
        password,
      })
      .then((response) => {
        if (response.data.accessToken) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }
        return response.data;
      });
  }

  login(email, password) {
    debugger;
    if (ISMOCK) {
      return axios
        .get(API_URL + "login?data.emailId=" + email)
        .then((response) => {
          localStorage.setItem("user", JSON.stringify(response.data[0]));
          return response.data[0];
        });
    }
    var basicAuth = "Basic " + btoa(email + ":" + password);
    return axios
      .post(API_URL + `login?emailId=${email}&password=${password}`, {
        params: {
          email,
          password,
        },
        headers: { Authorization: +basicAuth },
      })
      .then((response) => {
        if (response.data.code == 200) {
          localStorage.setItem("user", JSON.stringify(response.data));
          return response.data;
        } else if (response.data.code == 404) {
          throw new Error("Wrong Username OR Password entered!");
        }
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  register(data) {
    return axios.post(API_URL + "users", data);
  }
  registerCampus(data) {
    debugger;
    return axios.post(API_URL + "campus", data, axiosConfig);
  }
  registerCompany(data) {
    return axios.post(API_URL + "companies", data);
  }

  getCurrentUser() {
    //debugger;
    if (ISMOCK) {
      return axios.get(API_URL + "users1/").then((response) => {
        return response.data.data;
      });
    }

    const userStr = localStorage.getItem("user");
    if (userStr) {
      let user = JSON.parse(userStr);
      return axios
        .get(API_URL + "users/" + user.data.userId)
        .then((response) => {
          return response.data.data;
        });
    }
    return null;
  }

  getCampusUser() {
    //debugger;
    if (ISMOCK) {
      /*  return axios.get(API_URL + "campuslogin/").then((response) => {
        return response.data.data;
      }); */
      return axios.get(API_URL + "campuses/2").then((response) => {
        return response.data;
      });
    }

    const userStr = localStorage.getItem("user");
    if (userStr) {
      let user = JSON.parse(userStr);
      return axios
        .get(API_URL + "campus/" + user.data.userId)
        .then((response) => {
          return response.data.data;
        });
    }
    return null;
  }

  getSearchResult(searchKey, searchType) {
    var searchParam = "";
    switch (searchType) {
      case "student":
        searchParam = "users?firstName_like=" + searchKey;
        break;
      case "company":
        searchParam = "companies?company_like=" + searchKey;
        break;
      case "campus":
        searchParam = "campuses?campusName_like=" + searchKey;
        break;
      default:
        break;
    }
    if (ISMOCK) {
      if (searchParam != "") {
        return axios.get(API_URL + searchParam).then((response) => {
          return response.data;
        });
      }
    }

    return null;
  }

  getCompanyUser() {
    //debugger;
    if (ISMOCK) {
      return axios.get(API_URL + "companies/2").then((response) => {
        return response.data;
      });
    }

    const userStr = localStorage.getItem("user");
    if (userStr) {
      let user = JSON.parse(userStr);
      return axios
        .get(API_URL + "companies/" + user.data.userId)
        .then((response) => {
          return response.data.data;
        });
    }
    return null;
  }

  getLogin() {
    if (ISMOCK && 0) {
      return axios.get(API_URL + "login/").then((response) => {
        return response.data;
      });
    } else {
      const userStr = localStorage.getItem("user");
      return userStr ? JSON.parse(userStr) : null;
    }
  }

  checkLogin() {
    const userStr = localStorage.getItem("user");
    return userStr ? true : false;
  }
}

export default new AuthService();
