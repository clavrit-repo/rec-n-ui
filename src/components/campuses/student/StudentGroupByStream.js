const StudentGroupByStream = (props) => {
  return (
    <div className="col-xl-12 col-lg-12 col-md-12 mt-4">
      <div className="card shadow border-0 py-3 h-100">
        <div className="card-body">
          <div className="d-flex justify-content-between">
            <h4 className="fs-5 fw-light">
              <span className="textblue">5,400</span> Students
            </h4>
            <div>
              <img src="images/expand.png" alt="expand" className="w-75" />
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 col-md-6 mt-3">
              <div className="card shadow border-0 p-2 text-white success-card">
                <h4 className="mb-0">1,000</h4>
                <p className="fs-6 mb-0">MCA</p>
              </div>
            </div>
            <div className="col-lg-3 col-md-6 mt-3">
              <div className="card shadow border-0 p-2 text-white info-card">
                <h4 className="mb-0">1,700</h4>
                <p className="fs-6 mb-0">B.Tec</p>
              </div>
            </div>
            <div className="col-lg-3 col-md-6 mt-3">
              <div className="card shadow border-0 p-2 text-white success-card">
                <h4 className="mb-0">1,000</h4>
                <p className="fs-6 mb-0">MBA</p>
              </div>
            </div>
            <div className="col-lg-3 col-md-6 mt-3">
              <div className="card shadow border-0 p-2 text-white info-card">
                <h4 className="mb-0">1,700</h4>
                <p className="fs-6 mb-0">M.Tec</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default StudentGroupByStream;
