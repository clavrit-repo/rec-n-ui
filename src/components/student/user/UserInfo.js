import axios from "axios";
import { useState } from "react";
import authService, { API_URL } from "../../../services/auth-service";
import { flashMsg } from "../commonFunctions";
import $, { Callbacks } from "jquery";

/* function autoClose(){
  $(".alert").delay(3000).slideUp(200, function() {
    $(this).alert('close');
  });
} */

const UserInfo = (props) => {
  const { user } = props;
  const [msg, setMsg] = useState("");
  //const user=authService.getCurrentUser();

  const [firstName, setFirstName] = useState(user.firstName);
  const [lastName, setLastName] = useState(user.lastName);
  const [campus, setCampus] = useState(user.campus);
  const [country, setCountry] = useState(user.country);
  const [city, setCity] = useState(user.city);
  const [state, setState] = useState(user.state);
  const [phone, setPhone] = useState(user.phone);
  const [email, setEmail] = useState(user.emailId);

  const updateProfile = () => {
    debugger;
    setMsg("Updating the profile");
    axios
      .post(API_URL + `users/update`, {
        id: user.id,
        firstName,
        lastName,
        campus,
        country,
        state,
        city,
        phone,
        emailId: email,
      })
      .then((response) => {
        debugger;
        if (response.data.code == 200) {
          //localStorage.setItem("user", JSON.stringify(response.data.data));
          setMsg("profile updated !!");
          //autoClose();
          //document.getElementById("Cform").reset();
        }
      })
      .finally(() => {
        window.setTimeout(() => {
          setMsg("");
        }, 4000);
      });
  };

  function fileUpload() {
    const fd = new FormData(document.getElementById("imageForm"));
    let req = new Request("http://httpbin.org/post", {
      body: fd,
      method: "POST",
      contentType: "image/jpeg",
    });
    fetch(req)
      .then((response) => {
        //const reader = response.body.getReader();
        return response.json();
      })
      .then((data) => {
        document
          .getElementById("userImg")
          .setAttribute("src", data.files.fileName);
        document.getElementById("fileName").value = "";
      });
  }

  return (
    <div id="row1" className="mb-5 active">
      <div className="card shadow border-0 py-3 px-2">
        <div className="card-body">
          {msg && flashMsg(msg)}
          <h5 className="text-dark fw-light mb-4">Basic Information</h5>
          <div className="basicInfo d-flex mx-4 mb-5">
            <div className="w-25 me-5 text-center mt-5">
              <figure className="m-0 me-3">
                <img
                  id="userImg"
                  src="https://picsum.photos/200"
                  alt="User IMage "
                  className="w-100 border border-5 rounded-circle"
                />
              </figure>
              <button
                type="button"
                className="text-primary mt-5 bg-transparent border-0"
              >
                Replace Photo
              </button>
              <form id="imageForm">
                <input
                  type="file"
                  name="fileName"
                  id="fileName"
                  onChange={fileUpload}
                />
                <button
                  type="button"
                  className="text-primary mt-2 bg-transparent border-0"
                >
                  Upload Resume
                </button>
                <input
                  type="file"
                  name="fileName"
                  id="fileName"
                  onChange={fileUpload}
                />
              </form>

              <button
                className="btn btn-primary w-100 rounded-pill ms-1 py-2 mt-4 fs-5"
                type="button"
                onClick={() => updateProfile()}
              >
                Update Info
              </button>
            </div>
            <div className="w-75">
              <form className="row needs-validation" novalidate>
                <div className="col-md-6 position-relative mt-5">
                  <label
                    htmlFor="validationCustom01"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    First Name
                  </label>
                  <div className="border-start border-danger border-2 position-absolute"></div>
                  <input
                    type="text"
                    className="form-control fw-normal py-2 text-dark"
                    id="validationCustom01"
                    placeholder="Sandeep"
                    required
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                  />
                  <div className="valid-feedback">Looks good!</div>
                </div>
                <div className="col-md-6 position-relative mt-5">
                  <label
                    htmlFor="validationCustom01"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Last Name
                  </label>
                  <div className="border-start border-danger border-2 position-absolute"></div>
                  <input
                    type="text"
                    className="form-control fw-normal py-2 text-dark"
                    id="validationCustom02"
                    placeholder="Singh"
                    required
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                  />
                  <div className="valid-feedback">Looks good!</div>
                </div>
                <div className="col-md-6 position-relative mt-5">
                  <label
                    htmlFor="validationCustom04"
                    className="form-label form-labelImp text-muted"
                  >
                    Campus
                  </label>
                  <div className="border-start border-danger border-2 position-absolute"></div>
                  <select
                    className="form-select fw-normal py-2 text-dark"
                    id="validationCustom04"
                    required
                    value={campus}
                    onChange={(e) => setCampus(e.target.value)}
                  >
                    <option value="">Select Campus</option>
                    <option value="">IIT Delhi</option>
                    <option>IIT Kanpur</option>
                    <option>Punjab University Campus</option>
                    <option>Graphic Era University Dehradun</option>
                  </select>
                  <div className="invalid-feedback">
                    Please select a valid state.
                  </div>
                </div>
                <div className="col-md-6 position-relative mt-5">
                  <label
                    htmlFor="validationCustom04"
                    className="form-label form-labelImp text-muted"
                  >
                    Country
                  </label>
                  <div className="border-start border-danger border-2 position-absolute"></div>
                  <select
                    className="form-select fw-normal py-2 text-dark"
                    id="validationCustom04"
                    required
                    value={country}
                    onChange={(e) => setCountry(e.target.value)}
                  >
                    <option value="">Select</option>
                    <option>India</option>
                    <option>Nepal</option>
                  </select>
                  <div className="invalid-feedback">
                    Please select a valid state.
                  </div>
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    City
                  </label>
                  <input
                    type="text"
                    className="form-control fw-normal py-2 text-dark"
                    id="validationCustom02"
                    placeholder="Gurgaon"
                    required
                    value={city}
                    onChange={(e) => setCity(e.target.value)}
                  />
                  <div className="valid-feedback">Looks good!</div>
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    State
                  </label>
                  <input
                    type="text"
                    className="form-control fw-normal py-2 text-dark"
                    id="validationCustom02"
                    placeholder="Haryana"
                    required
                    value={state}
                    onChange={(e) => setState(e.target.value)}
                  />
                  <div className="valid-feedback">Looks good!</div>
                </div>

                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Email Address
                  </label>
                  <input
                    type="email"
                    className="form-control fw-normal py-2 text-dark"
                    id="validationCustom02"
                    placeholder="abc@gmail.com"
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                  <div className="valid-feedback">Looks good!</div>
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Phone Number
                  </label>
                  <input
                    type="number"
                    className="form-control fw-normal py-2 text-dark"
                    id="validationCustom02"
                    placeholder="9222555566"
                    required
                    value={phone}
                    onChange={(e) => setPhone(e.target.value)}
                  />
                  <div className="valid-feedback">Looks good!</div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default UserInfo;
