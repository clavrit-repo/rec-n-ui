import React, { Component, useEffect, useState } from "react";
import axios from "axios";
import authService, { API_URL } from "../../services/auth-service";
import { Link, useNavigate } from "react-router-dom";
import CampusView from "./CampusView";
import JobView from "./JobView";
import { AboutView, NewsView } from "./NewsView";
import AddJob from "./AddJob";
import AddCampus from "./AddCampus";
import UpdateCompany from "./UpdateCompany";
import UpdateCampus from "./UpdateCampus";
import UpdateJob from "./UpdateJob";

const Salary = () => {
  return (
    <div className="col-xl-12 col-lg-12 col-md-12 mt-4">
      <div className="card shadow border-0 h-100 px-2">
        <div className="card-body">
          <div className="row">
            <div className="col-xl-5 my-3">
              <h6 className="fw-light mb-4">Expected offered package</h6>
              <div>
                <input
                  type="text"
                  className="form-control fw-normal py-2 text-dark b"
                  placeholder="12K"
                />
              </div>
            </div>
            <div className="col-xl-7 my-3">
              <h6 className="fw-light mb-4">Expected skills from fresher</h6>
              <div className="border rounded tagsInput p-1">
                <div className="tag-container">
                  <input
                    type="text"
                    name="tags"
                    className="form-control"
                    data-role="tagsinput"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const CompanyProfile = (props) => {
  const { loggedIn } = props;
  const navigate = useNavigate();

  /*   const [isLogin] = useState(() => {
    return authService.getLogin();
  }); */

  const [isLogin] = useState(async () => {
    return await authService.getLogin();
  });

  const [company, setCompany] = useState(() => {
    authService.getCompanyUser().then((company) => {
      //setCompany(company.company);
      setCompany(company);
    });
  });
  const [openings, setOpenings] = useState(null);
  const [preCampuses, setPreCampuses] = useState(null);
  const [editCampus, setEditCampus] = useState(false);
  const [shareJob, setshareJob] = useState(false);

  useEffect(() => {
    //debugger;
    //if (isLogin == null || isLogin.data.userType !== "COMPANY") {
    if (isLogin == null) {
      navigate("/");
    } else {
      getOpenings();
      getCampuses();
    }
  }, [company]);

  const getOpenings = function () {
    axios
      .get("http://localhost:3004/openings")
      .then((response) => {
        return response.data;
      })
      .then((openings) => {
        setOpenings(openings);
      });
  };

  const getCampuses = function () {
    axios
      .get("http://localhost:3004/precampus")
      .then((response) => {
        return response.data;
      })
      .then((pcampus) => {
        setPreCampuses(pcampus);
      });
  };

  function deleteCampus(id) {
    debugger;
    axios.delete("http://localhost:3004/precampus/" + id).then((response) => {
      getCampuses();
    });
    //alert(id + "delete event trigger **************");
  }

  function deleteOpning(id) {
    debugger;
    axios.delete("http://localhost:3004/openings/" + id).then((response) => {
      getOpenings();
      //return response.data;
    });
  }

  function updateOpning(id, item) {
    axios.put("http://localhost:3004/openings/" + id, item).then((response) => {
      //return response.data;
      getOpenings();
    });
  }

  function onUpdate(id, data) {
    debugger;
    //alert(data);
    return axios.put(API_URL + "companies/" + id, data).then((response) => {
      console.log("Company Updated========", response.data);
      //setMsg("Company Profile Updated successfully !!!");
      setCompany(response.data);
      return response.data;
    });
  }

  function onUpdateCampus(item) {
    setEditCampus(item);
    //setShareCampus(item);
  }
  function onUpdateCampusPost(id, data) {
    return axios.put(API_URL + "precampus/" + id, data).then((response) => {
      getCampuses();
      return response.data;
    });
  }

  function updateShareJob(job) {
    setshareJob(job);
  }
  function UpdateJobPost(id, data) {
    debugger;
    return axios.put(API_URL + "openings/" + id, data).then((response) => {
      getOpenings();
      return response.data;
    });
  }

  function addCampus(item) {
    debugger;
    return axios
      .post("http://localhost:3004/precampus/", item)
      .then((response) => {
        getCampuses();
        return 1;
      });
  }

  function addJob(item) {
    debugger;
    return axios
      .post("http://localhost:3004/openings/", item)
      .then((response) => {
        getOpenings();
        return 1;
      });
  }

  return (
    <main className="bg-light">
      <section className="profileSec px-xl-5 px-lg-5 py-2">
        <div className="container-fluid">
          <div className="row profileView">
            <div className="col-xl-3 col-lg-6 col-md-6 mt-5">
              <div className="card shadow border-0 py-3 px-2 h-100">
                <div className="card-body">
                  <div className="cardhead">
                    <div>
                      <h4 className="textblue">{company?.companyName}</h4>
                    </div>
                    <figure className="m-0 mt-3">
                      <img src="/images/hcl.svg" alt className="w-100" />
                    </figure>
                  </div>
                  <div className="cardbody mt-2">
                    <div className="mb-2">
                      <h5 className="text-muted fw-light m-0">
                        {company?.firstName + " " + company?.lastName}
                      </h5>
                      <a className="text-primary fw-light">{company?.email}</a>
                    </div>
                    <div className>
                      <h6 className="text-muted fw-light"> {company?.phone}</h6>
                      <h6 className="text-muted">Gurgaon,Haryana,India</h6>
                    </div>
                    <div className="d-flex my-5">
                      <button
                        className="btn btn-primary py-2 w-100 rounded-pill ms-1 btn-sm"
                        type="submit"
                        data-bs-toggle="modal"
                        data-bs-target="#edit-details"
                      >
                        Edit Details
                      </button>
                    </div>
                  </div>
                </div>
                <div className="card-footer text-center bg-white border-0 text-muted text-small">
                  <p>
                    Member Since:{" "}
                    <span className="text-dark">August 18, 2021</span>
                  </p>
                </div>
              </div>
            </div>
            <div className="col-xl-6 col-lg-6 col-md-6">
              <div className="row">
                <CampusView
                  preCampuses={preCampuses}
                  onDelete={deleteCampus}
                  onUpdateCampus={onUpdateCampus}
                />
                <Salary />
                <AboutView />
                <NewsView />
              </div>
            </div>
            <div className="col-xl-3 col-lg-12 col-md-12 mt-5">
              <JobView
                openings={openings}
                onDelete={deleteOpning}
                onUpdate={updateShareJob}
              />
            </div>
          </div>
        </div>
      </section>
      {/* edit details modal */}
      {/* Modal */}
      {/* 
      <EditModel />
      <JobAddModel addJob={addJob} />
      <CampusAddModel addCampus={addCampus} /> */}
      {company && <UpdateCompany company={company} onUpdate={onUpdate} />}
      <AddJob addJob={addJob} />
      <UpdateJob shareJob={shareJob} doUpdate={UpdateJobPost} />

      <AddCampus addCampus={addCampus} />
      {editCampus && (
        <UpdateCampus
          campus={editCampus}
          //shareCampus={shareCampus}
          onUpdateCampusPost={onUpdateCampusPost}
        />
      )}
    </main>
  );
};
export default CompanyProfile;
