const About = () => {
    return (
      <div id="row8" className="mb-5">
        <div className="card shadow border-0 py-3 px-2">
          <div className="card-body">
            <h5 className="text-dark fw-light">About</h5>
            <div className="mt-4 border p-2 rounded" id="menu1 ">
              <p className="text-muted">
                Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                Laboriosam sint animi voluptatem deserunt rem illum accusamus,
                nostrum iste porro praesentium vel neque non consectetur tempora
                laborum ea reprehenderit adipisci fugit? Lorem, ipsum dolor sit
                amet consectetur adipisicing elit. Laboriosam sint animi
                voluptatem deserunt rem illum accusamus, nostrum iste porro
                praesentium vel neque non consectetur tempora laborum ea
                reprehenderit adipisci fugit?
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  };
  export default About;