import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";

import HeadLogo from "../includes/HeadLogo";
import TopHead from "../includes/TopHead";
import authService, { API_URL } from "../../services/auth-service";
import axios from "axios";
import BottomWidgets from "../includes/BottomWidgets";


const StudentRegister = ( props) => {

  const {loggedIn}=props;
  const [resMsg,setResmsg]  =  useState("");
  const [campuses,setCampuses]  =  useState("");
  const navigate=useNavigate();
  
  useEffect(()=>{
    //authService.getCamppus()
    axios.get(API_URL+"campus")
    .then((response)=>{
      setCampuses(response.data.data);
      console.log("******************** USER PROFILR",response);
    })
  },[]);


  const {
    register,
    watch,
    handleSubmit,
    formState : { errors }
  } = useForm();

  const fields=watch();
  console.log(fields);
  //console.log("form error",register,errors);

  function onSubmit(data) {
    alert(JSON.stringify(data));
    data.emailId=data.email;
    //data.password="admin@123";
    
    authService.register(data)
    .then(function(response){
        //alert(response.data);
        if(response.status===200 && response.data.message=='Successfully Register'){
      
            authService.login(data.email,data.password)
            .then((user)=>{
                if(user.data.userType=="USER"){
                  navigate('/student-profile');
                }
                if(user.data.userType=="CAMPUS"){
                  navigate('/campus-profile');
                }
                if(user.data.userType=="COMPANY"){
                  navigate('/company-profile');
                }  
            })
            .catch((error)=>{
              setResmsg(error.message);
            })
            //document.getElementById("formStudent").reset();
        }
        setResmsg(response.data.message);
    });
  } 


  return (
    <React.Fragment>
      <main>
            {/*    <TopHead loggedIn={loggedIn}/> */}
        <section className="mainSection vh-100">
         {/*  <HeadLogo /> */}
          <div className="container">
            <div className="row justify-content-center align-items-center mx-1 logInForm h-auto">
              <div className="col-lg-5 col-md-7 shadow p-5 bg-white mt-5">
              { resMsg && (
                   <div className="alert alert-success" role="alert">{resMsg}</div>
                   )}
                <h3 className="fw-normal">
                  <span className="me-2 iconLink">
                    <img src="images/student_icon.svg" alt="student_icon" />
                  </span>
                  Student Registration
                </h3>
                <form
                  id="formStudent" 
                  onSubmit={handleSubmit(onSubmit)}
                  className="row needs-validation"
                >
                  <div className="col-md-6 position-relative mt-5">
                    <label
                      htmlFor="validationCustom01"
                      className="form-label fw-normal text-muted form-labelImp"
                    >
                      First Name
                    </label>
                    <div className="border-start border-danger border-2 position-absolute"></div>
                    <input
                      type="text"
                      className="form-control fw-normal py-2 text-dark"
                      id="validationCustom01"
                      placeholder="Sandeep"
                      name="firstName"
                      {...register("firstName", { required: true } )} 
                    />
                    <input type="hidden" name="type" value="student"  {...register("type")} />
                    {errors.firstName && <div className="invalid-feedback d-block">This is required field</div>}
                  </div>
                  <div className="col-md-6 position-relative mt-5">
                    <label
                      htmlFor="validationCustom01"
                      className="form-label fw-normal text-muted form-labelImp"
                    >
                      Last Name
                    </label>
                    <div className="border-start border-danger border-2 position-absolute"></div>
                    <input
                      type="text"
                      className="form-control fw-normal py-2 text-dark"
                      placeholder="Singh"
                      name="lastName"
                      {...register("lastName",{ required: true })}
                    />
                   {errors.lastName && <div className="invalid-feedback d-block">This is required field</div>}
                  </div>
                  <div className="col-md-6 position-relative mt-5">
                    <label
                      htmlFor="validationCustom04"
                      className="form-label form-labelImp text-muted"
                    >
                      Campus
                    </label>
                    <div className="border-start border-danger border-2 position-absolute"></div>
                    <select
                      className="form-select fw-normal py-2 text-dark"
                      id="validationCustom04"
                      name="campus"
                      {...register("campus",{required:true})}
                    >
                      <option value="">Select Campus</option>
                      {campuses && campuses.map((c)=><option key={c.id} value={c.id}>{c.campusName}</option>)}
                      <option value="other">Other</option>
                    </select>
                    <input name="other" className="hide p2" style={{display:"none"}}  />
                    {errors.campus && <div className="invalid-feedback d-block"> Please select a valid campus.</div>}
                  </div>

                  <div className="col-md-6 position-relative mt-5">
                    <label
                      htmlFor="validationCustom04"
                      className="form-label form-labelImp text-muted"
                    >
                      Country
                    </label>
                    <div className="border-start border-danger border-2 position-absolute"></div>
                    <select
                      className="form-select fw-normal py-2 text-dark"
                      id="validationCustom04"
                      name="country"
                      {...register("country",{required:true})}
                    >
                      <option value=""> Select Country</option>
                      <option>India</option>
                      <option>...</option>
                    </select>
                    {errors.lastName && <div className="invalid-feedback d-block">Please select a valid country.</div>}
                  </div>
                  <div className="col-md-6 mt-5">
                    <label
                      htmlFor="validationCustom02"
                      className="form-label fw-normal text-muted form-labelImp"
                    >
                      City
                    </label>
                    <input
                      type="text"
                      className="form-control fw-normal py-2 text-dark"
                      placeholder="Gurgaon"
                      name="city"
                      {...register("city",{required:true})}
                    />
                   {errors.city && <div className="invalid-feedback d-block">This is required field.</div>}
                  </div>
                  <div className="col-md-6 mt-5">
                    <label
                      htmlFor="validationCustom02"
                      className="form-label fw-normal text-muted form-labelImp"
                    >
                      State
                    </label>
                    <input
                      type="text"
                      className="form-control fw-normal py-2 text-dark"
                      placeholder="Haryana"
                      name="state"
                      {...register("state",{required:true})}
                    />
                  {errors.state && <div className="invalid-feedback d-block">This is required field.</div>}
                  </div>

                  <div className="col-md-6 mt-5">
                    <label
                      htmlFor="validationCustom02"
                      className="form-label fw-normal text-muted form-labelImp"
                    >
                      Email Address
                    </label>
                    <input
                      type="email"
                      className="form-control fw-normal py-2 text-dark" 
                      placeholder="abc@gmail.com"
                      name="email"
                      {...register("email",{
                        required:true,
                        pattern:/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                     })}
                    />
                    {errors.email && <div className="invalid-feedback d-block">Required a valid email Address.</div>}
                  </div>
                  <div className="col-md-6 mt-5">
                    <label
                      htmlFor="validationCustom02"
                      className="form-label fw-normal text-muted form-labelImp"
                    >
                      Phone Number
                    </label>
                    <input
                      type="text"
                      className="form-control fw-normal py-2 text-dark"
                      placeholder="9222555566"
                      name="phone"
                      {...register("phone",{required:true,pattern:/^\d+$/})}
                      
                    />
                    {errors.phone && <div className="invalid-feedback d-block">Required a valid phone number.</div>}
                  </div>

                  <div className="col-md-6 mt-5">
                    <label
                      htmlFor="validationCustom02"
                      className="form-label fw-normal text-muted form-labelImp"
                    >
                    Password
                    </label>
                    <input
                      type="password"
                      className="form-control fw-normal py-2 text-dark"
                      placeholder="password"
                      name="password"
                      {...register("password",{required:true,minLength:4})}
                    />
                     {errors.password && <div className="invalid-feedback d-block">This is required field.</div>}
                  </div>
                  <div className="col-md-6 mt-5">
                    <label
                      htmlFor="validationCustom02"
                      className="form-label fw-normal text-muted form-labelImp"
                    >
                    Confirm Password
                    </label>
                    <input
                      type="password"
                      className="form-control fw-normal py-2 text-dark"
                      placeholder="Confirm your password"
                      name="cpassword"
                      {...register("cpassword",{ validate:(value) => fields.password === value})}
                      
                    />
                     {errors.cpassword && <div className="invalid-feedback d-block">This is required field.</div>}
                  </div>

                  <div className="col-12 d-flex mt-5">
                    <Link
                      className="btn btn-outline-secondary w-50 rounded-pill me-1"
                      type="button"
                      to="/"
                    >
                      Cancel
                    </Link>
                    <button
                      className="btn btn-primary w-50 rounded-pill ms-1"
                      type="submit"
                    >
                      Register
                    </button>
                  </div>
                </form>
              </div>
            </div>
           <BottomWidgets/>
          </div>
        </section>
      </main>
    </React.Fragment>
  );
};

export default StudentRegister;
