const HomeBudge = (props) => {
  const { title, count, icon } = props;
  return (
    <ul className="d-flex justify-content-between align-items-center p-4 shadow bg-white mx-auto">
      <li className="w-25">
        <img src={icon} alt="campus_icon" />
      </li>
      <li className="w-50 d-flex justify-content-end">
        <h2 className="font-weight-normal">{count}</h2>
        <h5 className="text-muted fw-light ms-4">{title}</h5>
      </li>
      <li className="w-25 d-flex justify-content-end right_arrow">
        <img src="images/right_arrow-icon.svg" alt="" />
      </li>
    </ul>
  );
};
export default HomeBudge;
