import axios from "axios";
import { useEffect, useState } from "react";
import authService, { API_URL } from "../../../services/auth-service";
import { soptions, statusClass, setLabel, flashMsg } from "../commonFunctions";
import ActionNode from "../ActionNode";

const CourseNode = (props) => {
  const { name, score } = props.data;
  const { id, onDelete, onUpdate } = props;
  return (
    <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
      <div className="w-75">
        <h6>{name}</h6>
        <h6 className={statusClass(parseInt(score))}>{setLabel(score)}</h6>
      </div>
      <ActionNode
        type="course"
        onDelete={onDelete}
        onUpdate={onUpdate}
        id={id}
      />
    </div>
  );
};

const Courses = (props) => {
  //const { addCourse, deleteCourse, courses } = props;
  //const user=authService.getCurrentUser();

  const [user, setUser] = useState(props.user);
  const [userCourses, setUserCourses] = useState(user ? user.courses : null);
  const [msg, setMsg] = useState("");

  useEffect(() => {
    getCourses();
  }, []);

  const [name, setName] = useState("");
  const [score, setScore] = useState("");
  const [isUpdate, setIsUpdate] = useState(false);
  const [isNotValid, setIsNotValid] = useState(false);

  function getCourses() {
    debugger;
    axios.get(API_URL + `users/${user.id}/course`).then((response) => {
      if (response.status == 200 || response.data.code == 200) {
        setUserCourses(response.data.data);
        //setUserCourses(response.data); for mock
      }
    });
  }

  function addCourse({ name, score }) {
    debugger;
    if (!name || !score) {
      setIsNotValid(1);
      return;
    }
    axios
      .post(API_URL + `users/${user.id}/course`, {
        name,
        score,
        userId: user.id,
      })
      .then((response) => {
        if (response.status == 201 || response.data.code == 200) {
          setMsg("course updated successfully !!!");
          setDefaults();
          getCourses();
        }
      })
      .catch((error) => {
        console.log(error.message);
      })
      .finally(hideAlert);
  }

  const deleteItem = (id) => {
    debugger;
    //axios.post(API_URL + `users/${user.id}/del_course/` + id + "/");
    axios
      .delete(API_URL + `course/` + id + "/")
      .then((response) => {
        if (response.status == 200 || response.data.code == 200) {
          setMsg("course Delted successfully !!!");
          setDefaults();
          getCourses();
        }
      })
      .catch((error) => {
        console.log(error.message);
      })
      .finally(hideAlert);
  };

  function updateItem(item) {
    const { id, name, score } = item;
    setName(name);
    setScore(score);
    setIsUpdate(id);
  }

  function setDefaults() {
    setName("");
    setScore("");
    setIsUpdate(false);
    setIsNotValid(false);
  }

  const hideAlert = () => {
    window.setTimeout(() => {
      setMsg("");
    }, 4000);
  };

  function updateCourse(id, item) {
    debugger;
    const nitem = { id, ...item, userId: user.id };
    //axios.post(API_URL + `users/${user.id}/score`, nitem);
    //axios.put(API_URL + `users/${user.id}/course/${id}`, nitem);

    axios
      .put(API_URL + `course/${id}`, nitem)
      .then((response) => {
        if (response.status == 200 || response.data.code == 200) {
          setMsg("Project Updated Successfully !!!");
          setDefaults();
          getCourses();
        }
      })
      .catch((error) => {
        console.log(error.message);
      })
      .finally(hideAlert);
  }

  return (
    <div id="row2" className="mb-5">
      <div className="card shadow border-0 py-3 px-2">
        <div className="card-body">
          {msg && flashMsg(msg)}
          <div className="blockMob d-flex justify-content-between align-items-start">
            <div className="">
              <h5 className="text-dark fw-light mb-4">Courses</h5>
              <form id="Coform" className="row needs-validation">
                <div className="col-md-12 mt-3">
                  <label
                    htmlFor="validationCustomUsername"
                    className="form-label fw-normal text-muted"
                  >
                    Course Name
                  </label>
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control fw-normal py-2 text-dark py-2"
                      placeholder="UI/UX Fundamental"
                      onChange={(e) => setName(e.target.value)}
                      value={name}
                    />
                  </div>
                  {!name && isNotValid && (
                    <span className="text-danger">This field is required</span>
                  )}
                </div>
                <div className="col-md-12 mt-5">
                  <label
                    htmlFor="validationCustomUsername"
                    className="form-label fw-normal text-muted"
                  >
                    Course Status
                  </label>
                  <div className="input-group">
                    <select
                      className="form-select text-dark fw-normal py-2"
                      aria-label="Default select example"
                      onChange={(e) => {
                        setScore(e.target.value);
                      }}
                      value={score}
                    >
                      <option value="">Select Status</option>
                      {soptions.map((i, idx) => (
                        <option key={idx} value={i.id}>
                          {i.text}
                        </option>
                      ))}
                    </select>
                  </div>
                  {!score && isNotValid && (
                    <span className="text-danger">This field is required</span>
                  )}
                  {!isUpdate && (
                    <button
                      className="fs-5 btn btn-primary w-100 rounded-pill ms-1 py-2 mt-4"
                      type="button"
                      onClick={() => addCourse({ name, score })}
                    >
                      Add
                    </button>
                  )}
                  {isUpdate && (
                    <span>
                      <button
                        className="btn btn-primary w-20 rounded-pill ms-1 py-2 mt-4 fs-5"
                        type="button"
                        style={{ width: "30%" }}
                        onClick={() =>
                          updateCourse(isUpdate, {
                            name,
                            score,
                          })
                        }
                      >
                        Update
                      </button>
                      <button
                        style={{ width: "40%" }}
                        className="btn btn-info w-30  rounded-pill ms-1 py-2 mt-4 fs-5"
                        type="button"
                        onClick={setDefaults}
                      >
                        Cancel Update
                      </button>
                    </span>
                  )}
                </div>
              </form>
            </div>
            <div className="w-50 rightblockMob">
              {userCourses &&
                userCourses.map((course, idx) => {
                  return (
                    <CourseNode
                      data={course}
                      key={idx}
                      onDelete={deleteItem}
                      onUpdate={() => updateItem(course)}
                      id={course.id}
                    />
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Courses;
