import axios from "axios";
import { useEffect, useState } from "react";
import authService, { API_URL } from "../../services/auth-service";
import { flashMsg } from "../common/commonFunctions";
import $, { Callbacks } from "jquery";
import { useForm } from "react-hook-form";

const UserInfo = (props) => {
  const { user } = props;
  const [msg, setMsg] = useState("");
  //const [user, setUser] = useState(props.user);

  /* useEffect(()=>{
      axios.get(API_URL+"campus/"+user.id)
      .then((data)=>{
        alert(data);
      })
    }) */
  //const user=authService.getCurrentUser();

  /*   const [firstName, setFirstName] = useState(user.firstName);
  const [lastName, setLastName] = useState(user.lastName);
  const [campusName, setCampus] = useState(user.campusName);
  const [ugcRank, setUgcRank] = useState(user.ugcRank);
  const [studentNo, setstudentNo] = useState(user.studentNo);
  const [comapnyCount, setComapnyCount] = useState(user.companyVisited);
  const [phone, setPhone] = useState(user.phone);
  const [email, setEmail] = useState(user.emailId); */

  const {
    campusName,
    ugcRank,
    studentNo,
    companyVisited,
    phone,
    emailId: email,
  } = user;

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    defaultValues: {
      campusName,
      ugcRank,
      studentNo,
      companyVisited,
      phone,
      email,
    },
  });

  const onSubmit = function (data) {
    //alert(JSON.stringify(data));
    debugger;
    const {
      campusName,
      studentNo: studentNo,
      companyVisited: companyVisited,
      ugcRank,
      phone,
      email: emailId,
    } = data;

    axios
      .put(API_URL + `campuses/2`, {
        id: user.id,
        campusName,
        studentNo,
        companyVisited,
        ugcRank,
        phone,
        emailId,
      })
      .then((result) => {
        setMsg("profile updated successfully!!");
      })
      .finally(() => {
        window.setTimeout(() => {
          setMsg("");
        }, 3000);
      });
  };

  /*   const updateProfile = () => {
    debugger;
    setMsg("Updating the profile");
    axios
      .post(API_URL + `campus/update`, {
        id: user.id,
        firstName,
        lastName,
        campusName,
        studentNo: studentNo,
        companyVisited: comapnyCount,
        ugcRank,
        phone,
        emailId: email,
      })
      .then((response) => {
        debugger;
        if (response.data.code == 200) {
          //localStorage.setItem("user", JSON.stringify(response.data.data));
          setMsg("profile updated !!");
          //autoClose();
          //document.getElementById("Cform").reset();
        }
      })
      .finally(() => {
        window.setTimeout(() => {
          setMsg("");
        }, 4000);
      });
  }; */

  return (
    <div id="row1" className="mb-5 active">
      <form className="row needs-validation" onSubmit={handleSubmit(onSubmit)}>
        <div className="card shadow border-0 py-3 px-2">
          <div className="card-body">
            {msg && flashMsg(msg)}
            <h5 className="text-dark fw-light mb-4">
              Campus Basic Information
            </h5>
            <div className="basicInfo d-flex mx-4 mb-5">
              <div className="w-25 me-5 text-center mt-5">
                <figure className="m-0 me-3">
                  <img
                    src="https://picsum.photos/200"
                    alt=""
                    className="w-100 border border-5 rounded-circle"
                  />
                </figure>
                <button
                  type="button"
                  className="text-primary mt-5 bg-transparent border-0"
                >
                  Replace Photo
                </button>
                <button
                  className="btn btn-primary w-100 rounded-pill ms-1 py-2 mt-4 fs-5"
                  type="submit"
                  /* onClick={() => updateProfile()} */
                >
                  Update Info
                </button>
              </div>
              <div className="w-75 row">
                {/*  <div className="col-md-6 position-relative mt-5">
                    <label
                      htmlFor="validationCustom01"
                      className="form-label fw-normal text-muted form-labelImp"
                    >
                     First Name
                    </label>
                    <div className="border-start border-danger border-2 position-absolute"></div>
                    <input
                      type="text"
                      className="form-control fw-normal py-2 text-dark"
                      id="validationCustom01"
                      placeholder="Sandeep"
                      required
                      value={firstName}
                      onChange={(e)=>setFirstName(e.target.value)}
                    />
                    <div className="valid-feedback">Looks good!</div>
                  </div> */}
                {/* <div className="col-md-6 position-relative mt-5">
                    <label
                      htmlFor="validationCustom01"
                      className="form-label fw-normal text-muted form-labelImp"
                    >
                    Last Name
                    </label>
                    <div className="border-start border-danger border-2 position-absolute"></div>
                    <input
                      type="text"
                      className="form-control fw-normal py-2 text-dark"
                      id="validationCustom02"
                      placeholder="Singh"
                      required
                      value={lastName}
                      onChange={(e)=>setLastName(e.target.value)}
                    />
                    <div className="valid-feedback">Looks good!</div>
                  </div> */}

                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Campus Name
                  </label>
                  <input
                    type="text"
                    className="form-control fw-normal py-2 text-dark"
                    id="validationCustom02"
                    placeholder="IIT DELHI"
                    /*    value={campusName} */
                    name="campusName"
                    {...register("campusName", { required: true })}
                  />
                  {errors.campusName && (
                    <div className="invalid-feedback d-block">
                      This is required field
                    </div>
                  )}
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    UGC Rank
                  </label>
                  <input
                    type="text"
                    className="form-control fw-normal py-2 text-dark"
                    id="validationCustom02"
                    placeholder="3"
                    name="ugcRank"
                    {...register("ugcRank", { required: true })}
                  />
                  {errors.ugcRank && (
                    <div className="invalid-feedback d-block">
                      This is required field
                    </div>
                  )}
                </div>

                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Total Students
                  </label>
                  <input
                    type="text"
                    className="form-control fw-normal py-2 text-dark"
                    id="validationCustom02"
                    placeholder="1000K"
                    name="studentNo"
                    {...register("studentNo", { required: true })}
                  />
                  {errors.studentNo && (
                    <div className="invalid-feedback d-block">
                      This is required field
                    </div>
                  )}
                </div>

                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Number of Company Visited
                  </label>
                  <input
                    type="text"
                    className="form-control fw-normal py-2 text-dark"
                    id="validationCustom02"
                    placeholder="10"
                    name="companyVisited"
                    {...register("companyVisited", { required: true })}
                  />
                  {errors.companyVisited && (
                    <div className="invalid-feedback d-block">
                      This is required field
                    </div>
                  )}
                </div>

                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Email Address
                  </label>
                  <input
                    type="email"
                    className="form-control fw-normal py-2 text-dark"
                    id="validationCustom02"
                    placeholder="abc@gmail.com"
                    name="email"
                    {...register("email", { required: true })}
                  />
                  {errors.email && (
                    <div className="invalid-feedback d-block">
                      Required a valid email Address.
                    </div>
                  )}
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Phone Number
                  </label>
                  <input
                    type="number"
                    className="form-control fw-normal py-2 text-dark"
                    id="validationCustom02"
                    placeholder="9222555566"
                    name="phone"
                    {...register("phone", { required: true })}
                  />
                  {errors.phone && (
                    <div className="invalid-feedback d-block">
                      Required a valid Phone number.
                    </div>
                  )}
                  <div className="valid-feedback">Looks good!</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};
export default UserInfo;
