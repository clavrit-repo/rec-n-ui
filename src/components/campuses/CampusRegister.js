import { useState } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import authService from "../../services/auth-service";
import HeadBeforeLogin from "../includes/HeadBeforLogin";
import TopHead from "../includes/TopHead";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import BottomWidgets from "../includes/BottomWidgets";

const CampusRegister = (props) => {
  const { loggedIn } = props;
  const [resMsg, setResmsg] = useState("");
  const navigate = useNavigate();

  const validationSchema = yup.object().shape({
    campusName: yup.string().required(),
    campusEmail: yup.string().email().required("Email is requird"),
    ugcRank: yup
      .number(0)
      .required("Ugc Rank is required")
      .typeError("Must be a number"),
    studentCount: yup
      .number()
      .required()
      .positive()
      .default(0)
      .typeError("Must be a number"),
    companyCount: yup.number().required().typeError("Must be a number"),
    phone: yup
      .number()
      .required("Phone is requird")
      .typeError("Must be a number"),
    password: yup
      .string()
      .required("Password is required")
      .min(6, "Password must be at least 6 characters"),
    cpassword: yup
      .string()
      .required()
      .oneOf([yup.ref("password")], "Password must match"),
  });

  const formOptions = { resolver: yupResolver(validationSchema) };

  const {
    register,
    watch,
    handleSubmit,
    formState: { errors },
  } = useForm(formOptions);

  function onSubmit(data) {
    debugger;
    alert(JSON.stringify(data));
    const {
      campusName,
      campusEmail: emailId,
      password,
      phone,
      ugcRank,
      studentCount: studentNo,
      companyCount: companyVisited,
    } = data;

    authService
      .registerCampus({
        campusName,
        emailId,
        password,
        phone,
        ugcRank,
        studentNo,
        companyVisited,
      })
      .then(function (response) {
        //alert(response.data);
        if (response.status === 200) {
          authService
            .login(emailId, password)
            .then((user) => {
              if (user.data.userType == "USER") {
                navigate("/student-profile");
              }
              if (user.data.userType == "CAMPUS") {
                navigate("/campus-profile");
              }
              if (user.data.userType == "COMPANY") {
                navigate("/company-profile");
              }
            })
            .catch((error) => {
              setResmsg(error.message);
            });
          setResmsg(
            "Campus Registered successfully!.Pls Login to update Profile"
          );
          //document.getElementById("formCampus").reset();
        }
      })
      .catch((error) => {
        console.log(error.message);
      });
  }

  return (
    <main>
      <section className="mainSection vh-100">
        <div className="container">
          <div className="row justify-content-center align-items-center mx-1 logInForm h-auto">
            <div className="col-lg-5 col-md-7 shadow p-5 bg-white mt-5">
              {resMsg && (
                <div className="alert alert-success" role="alert">
                  {resMsg}
                </div>
              )}
              <h3 className="fw-normal">
                <span className="me-2 iconLink">
                  <img src="images/campus_icon.svg" alt />
                </span>
                Campus Registration
              </h3>
              <form
                className="row needs-validation"
                id="formCampus"
                onSubmit={handleSubmit(onSubmit)}
              >
                <div className="col-md-6 position-relative mt-5">
                  <label
                    htmlFor="validationCustom01"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Campus Name
                  </label>
                  <div className="border-start border-danger border-2 position-absolute" />
                  <input
                    type="text"
                    className="form-control fw-normal py-2 text-dark"
                    id="validationCustom01"
                    placeholder="IIT Delhi"
                    name="campusName"
                    {...register("campusName", { required: true })}
                  />
                  <input
                    type="hidden"
                    name="type"
                    value="campus"
                    {...register("type")}
                  />
                  <div
                    className={`invalid-feedback ${
                      errors.campusName ? " d-block" : ""
                    }`}
                  >
                    {errors.campusName?.message}
                  </div>
                </div>
                <div className="col-md-6 position-relative mt-5">
                  <label
                    htmlFor="validationCustom01"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    UGC Rank
                  </label>
                  <div className="border-start border-danger border-2 position-absolute" />
                  <input
                    type="text"
                    className="form-control fw-normal py-2 text-dark"
                    placeholder={3}
                    name="ugcRank"
                    {...register("ugcRank", { required: true })}
                  />
                  <div
                    className={`invalid-feedback ${
                      errors.ugcRank ? " d-block" : ""
                    }`}
                  >
                    {errors.ugcRank?.message}
                  </div>
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Total Students
                  </label>
                  <input
                    type="text"
                    className="form-control fw-normal py-2 text-dark"
                    placeholder="Enter Total Students"
                    name="studentCount"
                    {...register("studentCount", { required: true })}
                  />
                  <div
                    className={`invalid-feedback ${
                      errors.studentCount ? " d-block" : ""
                    }`}
                  >
                    {errors.studentCount?.message}
                  </div>
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom09"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Companies Visited
                  </label>
                  <input
                    type="text"
                    className="form-control fw-normal py-2 text-dark"
                    id="validationCustom09"
                    placeholder="Enter Companies visited"
                    name="companyCount"
                    {...register("companyCount", { required: true })}
                  />
                  <div
                    className={`invalid-feedback ${
                      errors.companyCount ? " d-block" : ""
                    }`}
                  >
                    {errors.companyCount?.message}
                  </div>
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Email Address
                  </label>
                  <input
                    type="email"
                    className="form-control fw-normal py-2 text-dark"
                    placeholder="abc@gmail.com"
                    name="campusEmail"
                    {...register("campusEmail", { required: true })}
                  />
                  <div
                    className={`invalid-feedback ${
                      errors.campusEmail ? " d-block" : ""
                    }`}
                  >
                    {errors.campusEmail?.message}
                  </div>
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Phone Number
                  </label>
                  <input
                    type="text"
                    className="form-control fw-normal py-2 text-dark"
                    placeholder={9222555566}
                    name="phone"
                    {...register("phone", { required: true })}
                  />
                  <div
                    className={`invalid-feedback ${
                      errors.phone ? " d-block" : ""
                    }`}
                  >
                    {errors.phone?.message}
                  </div>
                </div>

                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Password
                  </label>
                  <input
                    type="password"
                    className="form-control fw-normal py-2 text-dark"
                    placeholder="password"
                    name="password"
                    {...register("password", { required: true })}
                  />
                  <div
                    className={`invalid-feedback ${
                      errors.password ? " d-block" : ""
                    }`}
                  >
                    {errors.password?.message}
                  </div>
                </div>
                <div className="col-md-6 mt-5">
                  <label
                    htmlFor="validationCustom02"
                    className="form-label fw-normal text-muted form-labelImp"
                  >
                    Confirm Password
                  </label>
                  <input
                    type="password"
                    className="form-control fw-normal py-2 text-dark"
                    placeholder="Confirm your password"
                    name="cpassword"
                    {...register("cpassword", { required: true })}
                  />
                  <div
                    className={`invalid-feedback ${
                      errors.cpassword ? " d-block" : ""
                    }`}
                  >
                    {errors.cpassword?.message}
                  </div>
                </div>
                <div className="col-12 d-flex mt-5">
                  <button
                    className="btn btn-outline-secondary w-50 rounded-pill me-1"
                    type="button"
                  >
                    Cancel
                  </button>
                  <button
                    className="btn btn-primary w-50 rounded-pill ms-1"
                    type="submit"
                  >
                    Register
                  </button>
                </div>
              </form>
            </div>
          </div>
          <BottomWidgets />
        </div>
      </section>
    </main>
  );
};

export default CampusRegister;
