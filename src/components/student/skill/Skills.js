import { useEffect, useState } from "react";
import authService, { API_URL } from "../../../services/auth-service";
import $ from "jquery";
import axios from "axios";
import { flashMsg } from "../commonFunctions";

const Skills = (props) => {
  debugger;
  const [user, setUser] = useState(props.user);
  const [msg, setMsg] = useState("");
  const [userSkills, setUserSkills] = useState(user.skills);

  useEffect(() => {
    /*   authService.getCurrentUser()
      .then((user)=>{
        setUserSkills(user.skills);
      }); */

    //if(userSkills && userSkills.length){
    if (userSkills?.length) {
      window.$("input.skills-tags").tagsinput();
      userSkills.split(",").forEach((skill) => {
        console.log("item ==========", skill);
        window.$("input.skills-tags").tagsinput("add", skill);
      });
    } else {
      window.$("input.skills-tags").tagsinput();
    }
  }, []);

  function updateItem1() {
    debugger;
    let items = $("input.skills-tags").val().split(",");
    console.log(items);
    let allItems = [];
    if (items) {
      allItems = items.map((ele, idx) => {
        return { id: idx, name: ele };
      });
    }
    console.log(allItems);

    let { id, firstName, lastName, emailId } = user;
    setMsg("Updating the Skills .....");
    axios
      .put(API_URL + `users/update`, {
        id,
        /*  firstName,
            lastName ,
            emailId, */
        skills: allItems,
      })
      .then((response) => {
        if (response.data.code == 200) {
          setMsg("Skills Updates successfully !!");
          //setUserProjects(projects);
        }
      })
      .finally(() => {
        window.setTimeout(() => {
          setMsg("");
        }, 4000);
      });
  }

  function updateItem() {
    debugger;
    let items = $("input.skills-tags").val();
    console.log(items);
    let { id, firstName, lastName, emailId } = user;
    setMsg("Updating the Skills .....");
    axios
      .post(API_URL + `users/update`, {
        id,
        emailId,
        firstName,
        lastName,
        skills: items,
      })
      .then((response) => {
        if (response.data.code == 200) {
          setMsg("Skills Updates successfully !!");
          //setUserProjects(projects);
        }
      })
      .finally(hideAlert);
  }

  const hideAlert = () => {
    window.setTimeout(() => {
      setMsg("");
    }, 4000);
  };

  return (
    <div id="row5" className="mb-5">
      <div className="card shadow border-0 py-3 px-2">
        <div className="card-body">
          {msg && flashMsg(msg)}
          <h5 className="text-dark fw-light mb-4">Skills</h5>
          <div className="border rounded p-1 my-4">
            <div className="tag-container">
              <input
                type="text"
                name="tags"
                className="form-control skills-tags"
                data-role="tagsinput"
              />
              <button
                className="btn btn-primary"
                style={{ position: "absolute", right: "30px" }}
                onClick={updateItem}
              >
                Update
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Skills;
