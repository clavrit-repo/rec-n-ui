import { useState } from "react";
import $ from "jquery";

const AddCampus = (props) => {
  //debugger;
  const { addCampus, isUpdate } = props;
  const [campus, setCampus] = useState("");
  const [invalid, setInvalid] = useState(false);
  const [msg, setMsg] = useState(false);

  const submit = () => {
    debugger;
    if (!campus) {
      setInvalid(true);
      setMsg(false);
    } else {
      addCampus({ name: campus }).then((result) => {
        setCampus(""); // set to defualt
        setInvalid(false);
        setMsg("New Campus Added successfully!");
        //window.$("#campus-add-model").modal("hide");
      });
    }
  };

  return (
    <div
      className="modal fade"
      id="campus-add-model"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabIndex={-1}
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content rounded-0">
          <div className="modal-header border-0">
            <h5 className="modal-title" id="staticBackdropLabel">
              Add New Campus
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            />
          </div>
          <div className="modal-body px-4">
            {msg && <p className="text-danger text-center">{msg}</p>}
            <form className="row">
              <div className="col-md-12 position-relative mt-lg-2 mt-5 ">
                <label
                  htmlFor="validationCustom01"
                  className="form-label fw-normal text-muted"
                >
                  Campus Name
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  id="val"
                  placeholder=""
                  value={campus}
                  onChange={(e) => setCampus(e.target.value)}
                />
              </div>
              {invalid && !campus && (
                <p className="text-danger">This field is required</p>
              )}
              <div className="col-12 d-flex my-5">
                <button
                  className="btn btn-outline-secondary w-50 rounded-pill me-1"
                  data-bs-dismiss="modal"
                  type="button"
                >
                  Cancel
                </button>
                <button
                  className="btn btn-primary w-50 rounded-pill ms-1"
                  type="button"
                  /*  data-bs-dismiss="modal" */
                  /* onClick={() =>
                    addCampus({
                      name: campus,
                    })
                  } */
                  onClick={submit}
                >
                  Register
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddCampus;
