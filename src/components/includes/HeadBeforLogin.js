import { Link } from "react-router-dom";

const HeadBeforeLogin = () => {
  return (
    <>
      <header>
        <nav className="navbar navbar-light py-3 px-5">
          <Link to="/" className="navbar-brand me-0">
            <img src="images/logo.jpeg" alt="logo" className="w-75" />
          </Link>
          <form className="form-inline d-flex">
            <div className="dropdown">
              <button
                className="dropdown btn btn-common px-4 me-2 fw-light text-dark"
                id="navbarDropdown"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Register
                <span className="ms-2 iconLink">
                  <img src="images/dropdown_icon.svg" alt="" />
                </span>
              </button>
              <ul
                className="dropdown-menu mt-3 iconLink"
                aria-labelledby="navbarDropdown"
              >
                 <li>
                  <Link
                    className="dropdown-item fw-light px-3 py-2"
                    to="student-register"
                  >
                    <span className="me-3">
                      <img src="images/student_icon.svg" alt="student_icon" />
                    </span>
                    Student Register
                  </Link>
                </li>
                <li>
                  <Link
                    className="dropdown-item fw-light px-3 py-2"
                    to="/campus-registration"
                  >
                    <span className="me-3">
                      <img src="images/campus_icon.svg" alt="campus_icon" />
                    </span>{" "}
                    Register Your Campus
                  </Link>
                </li>
                <li>
                  <Link
                    className="dropdown-item fw-light px-3 py-2"
                    to="company-registration"
                  >
                    <span className="me-3">
                      <img src="images/company_icon.svg" alt="company_icon" />
                    </span>
                    Company Registration
                  </Link>
                </li>
               
              </ul>
            </div>
            <button type="button" className="btn btn-common px-4">
              <Link to="sign-in">Sign In</Link>
            </button>
          </form>
        </nav>
      </header>
    </>
  );
};

export default HeadBeforeLogin;
