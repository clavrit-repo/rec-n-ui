const CampusCard = function ({ campuses, onDelete, onUpdate }) {
  if (!campuses) {
    return null;
  }
  return campuses?.map((campus) => {
    return (
      <div className="col-xl-4 col-md-12 mt-3 px-2">
        <div className="h-100 p-3 shadow d-flex justify-content-between mb-3 rounded align-items-center">
          <div className="w-100">
            {/*  <img src="images/blank.svg" class="box-width" /> */}
            <p className="fs-6 ms-0 mb-0 text-dark">{campus.company}</p>
          </div>
          <div className="d-flex justify-content-end ms-2">
            <div className="dropdown">
              <button
                className="click border-0 bg-transparent"
                /* data-row-id="dropdownMenuButton-7" */
                data-row-id={"menu-" + campus.id}
              >
                <img src="images/threeDots_img.png" alt />
              </button>
              <ul
                /*  className="list p-0 dropdownMenuButton-7" */
                className={`list p-0 menu-${+campus.id}`}
              >
                <li>
                  <a
                    data-bs-toggle="modal"
                    data-bs-target="#job-update-modal"
                    className="dropdown-item"
                    href="#"
                    /*  onClick={onUpdate(campus.id)} */
                    onClick={(e) => {
                      e.preventDefault();
                      onUpdate(campus);
                    }}
                  >
                    Edit
                  </a>
                </li>
                <li>
                  <a
                    className="dropdown-item"
                    href="#"
                    onClick={(e) => {
                      e.preventDefault();
                      onDelete(campus.id);
                    }}
                  >
                    Delete
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  });
};

const ViewCompanies = (props) => {
  const { companies, onUpdate, onDelete } = props;
  return (
    <div className="col-xl-12 col-lg-12 col-md-12 mt-5">
      <div className="card shadow border-0 py-3 px-2 h-100">
        <div className="card-body">
          <div className="d-flex justify-content-between">
            <h4 className="fs-5 fw-light">
              <span className="textblue">{companies?.length}</span> Companies
              Visited
            </h4>
            <button
              className="border-0 bg-transparent"
              data-bs-toggle="modal"
              data-bs-target="#job-add-model"
            >
              <img
                src="images/plus_icon.svg"
                className="mx-2 plus_icon"
                alt="plus_icon"
              />
            </button>
          </div>
          <div className="row justify-content-between">
            <CampusCard
              campuses={companies}
              onUpdate={onUpdate}
              onDelete={onDelete}
            />
          </div>
        </div>
      </div>
    </div>
  );
};
export default ViewCompanies;
