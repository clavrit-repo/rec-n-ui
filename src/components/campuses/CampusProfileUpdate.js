import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import authService from "../../services/auth-service";
import LeftNavBar from "./nav/LeftNavBar";
import ProfileEdit from "./profile/ProfileEdit";
import UserInfo from "./UserInfo";

const CampusProfileUpdate = (props) => {
  //const { loggedIn } = props;
  const navigate = useNavigate();

  const [isLogin, setLoggedIn] = useState(() => {
    return authService.getLogin();
  });
  const [campus, setCampus] = useState(null);

  useEffect(() => {
    //debugger;
    if (isLogin == null) {
      navigate("/");
    } else {
      debugger;
      authService.getCampusUser().then((user) => {
        setCampus(user);
        console.log("******************** STU PROFILR", user);
      });
    }
  }, []);

  if (!campus) return false;
  else
    return (
      <React.Fragment>
        <section className="profileViewSec py-5">
          <div className="container">
            <div className="row profileView">
              <LeftNavBar />
              <div className="col-md-9 mt-5 h-100">
                <ProfileEdit user={campus} />
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
};

export default CampusProfileUpdate;
