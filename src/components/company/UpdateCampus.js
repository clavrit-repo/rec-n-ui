import { useEffect, useState } from "react";
import $ from "jquery";

const UpdateCampus = (props) => {
  debugger;
  const { onUpdateCampusPost, campus } = props;
  const [campusName, setCampusName] = useState(campus.name);
  const [invalid, setInvalid] = useState(false);
  const [msg, setMsg] = useState(false);

  useEffect(() => {
    setCampusName(campus.name);
  }, [campus]);

  const onSubmit = () => {
    debugger;
    if (!campusName) {
      setInvalid(true);
      setMsg(false);
    } else {
      onUpdateCampusPost(campus.id, {
        name: campusName,
      }).then((result) => {
        setCampusName(""); // set to defualt
        setInvalid(false);
        setMsg("Campus Updated Successfully!");
      });
      //UpdateCampus(campus).then((result) => {});
    }
  };

  return (
    <div
      className="modal fade"
      id="campus-update-modal"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabIndex={-1}
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content rounded-0">
          <div className="modal-header border-0">
            <h5 className="modal-title" id="staticBackdropLabel">
              Update Campus
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            />
          </div>
          <div className="modal-body px-4">
            {msg && <p className="text-danger text-center">{msg}</p>}
            <form className="row">
              <div className="col-md-12 position-relative mt-lg-2 mt-5 ">
                <label
                  htmlFor="validationCustom01"
                  className="form-label fw-normal text-muted"
                >
                  Campus Name
                </label>
                <input
                  type="text"
                  className="form-control fw-bold text-dark"
                  id="val"
                  placeholder=""
                  value={campusName}
                  onChange={(e) => setCampusName(e.target.value)}
                />
              </div>
              {invalid && !campusName && (
                <p className="text-danger">This field is required</p>
              )}
              <div className="col-12 d-flex my-5">
                <button
                  className="btn btn-outline-secondary w-50 rounded-pill me-1"
                  data-bs-dismiss="modal"
                  type="button"
                >
                  Cancel
                </button>
                <button
                  className="btn btn-primary w-50 rounded-pill ms-1"
                  type="button"
                  /*  data-bs-dismiss="modal" */
                  /* onClick={() =>
                    UpdateCampus({
                      name: campus,
                    })
                  } */
                  onClick={onSubmit}
                >
                  Register
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UpdateCampus;
