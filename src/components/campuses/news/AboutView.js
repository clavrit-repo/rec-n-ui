const AboutView = function () {
  return (
    <div className="col-xl-12 col-lg-12 col-md-12 mt-4">
      <div className="card shadow border-0 px-2 h-100">
        <div className="card-body">
          <h4 className="fs-5 fw-light">About</h4>
          <div className="mt-3" id="menu1">
            <ul className="term-list p-0">
              <li className="term-item">
                <p className="text-muted">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Laboriosam sint animi voluptatem deserunt rem illum accusamus,
                  nostrum iste porro praesentium vel neque non consectetur
                  tempora laborum ea reprehenderit adipisci fugit?
                </p>
              </li>
              <li className="term-item">
                <p className="moretext text-muted">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Laboriosam sint animi voluptatem deserunt rem illum accusamus,
                  nostrum iste porro praesentium vel neque non consectetur
                  tempora laborum ea reprehenderit adipisci fugit?
                </p>
              </li>
              <li className="term-item">
                <a className="moreless-button justify-content-end text-muted">
                  ...see more
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
export default AboutView;
