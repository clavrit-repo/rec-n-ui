import axios from "axios";
import { useEffect, useState } from "react";
import authService, { API_URL } from "../../../services/auth-service";
import ActionNode from "../ActionNode";
import { flashMsg, studentStreams as streams } from "../commonFunctions";

const QualificationNode = (props) => {
  const { name, score } = props.data;
  const { id, onDelete, onUpdate } = props;
  return (
    <div className="col-md-6 mb-3">
      <div className="shadow p-3 d-flex justify-content-between mb-3 rounded align-items-center">
        <div className="w-75">
          <h6>{name}</h6>
          <h6 className="text-warning">{score + "%"}</h6>
        </div>
        <ActionNode
          id={id}
          type="qualification"
          onDelete={onDelete}
          onUpdate={onUpdate}
        />
      </div>
    </div>
  );
};

const Qualification = (props) => {
  const [user, setUser] = useState(props.user);
  const [msg, setMsg] = useState("");
  const [userStreams, setUserStreams] = useState(
    user ? user.qualifications : null
  );

  const [name, setName] = useState("");
  const [score, setScore] = useState("");
  const [isUpdate, setIsUpdate] = useState(false);
  const [isNotValid, setIsNotValid] = useState(false);

  useEffect(() => {
    setUser(props.user);
    getStreams();
  }, [props.user]);

  function getStreams() {
    axios.get(API_URL + `users/${user.id}/qualification`).then((response) => {
      if (response.status == 200 || response.data.code == 200) {
        setUserStreams(response.data.data);
        //setUserStreams(response.data);
      }
    });
  }

  function addQualification({ name, score }) {
    if (!name || !score) {
      setIsNotValid(1);
      return;
    }

    axios
      .post(API_URL + `users/${user.id}/qualification`, {
        name,
        score,
        userId: user.id,
      })
      .then((response) => {
        if (response.status == 201 || response.data.code == 200) {
          setMsg("Qualification Inserted Successfully !!!");
          //document.getElementById("Pform").reset();
          setDefaults();
          getStreams();
        }
      })
      .catch((error) => {
        console.log(error.message);
      })
      .finally(hideAlert);
  }

  const deleteItem = (id) => {
    /*  axios
    .post(API_URL + `users/${user.id}/del_qualification/` + id + "/") */
    axios
      .delete(API_URL + `qualification/` + id + "/")
      .then((response) => {
        if (response.status == 200 || response.data.code == 200) {
          setMsg("Qualification Delted successfully !!!");
          setDefaults();
          getStreams();
        }
      })
      .catch((error) => {
        console.log(error.message);
      })
      .finally(hideAlert);
  };

  function updateItem(item) {
    const { id, name, score } = item;
    setName(name);
    setScore(score);
    setIsUpdate(id);
  }

  function setDefaults() {
    setName("");
    setScore("");
    setIsUpdate(false);
    setIsNotValid(false);
  }

  const hideAlert = () => {
    window.setTimeout(() => {
      setMsg("");
    }, 4000);
  };

  function updateQualification(id, item) {
    debugger;
    const nitem = { id, ...item, userId: user.id };
    //axios.post(API_URL + `users/${user.id}/qualification`, nitem);
    axios
      .put(API_URL + `qualification/${id}`, nitem)
      .then((response) => {
        if (response.status == 200 || response.data.code == 200) {
          setMsg("Qualification Updated Successfully !!!");
          setDefaults();
          getStreams();
        }
      })
      .catch((error) => {
        console.log(error.message);
      })
      .finally(hideAlert);
  }

  return (
    <div id="row7" className="mb-5">
      <div className="card shadow border-0 py-3 px-2">
        <div className="card-body">
          {msg && flashMsg(msg)}
          <div className="blockMob d-flex justify-content-between align-items-start">
            <div className="">
              <h5 className="text-dark fw-light mb-4">Qualification</h5>
              <form id="Qform" className="row needs-validation" novalidate>
                <div className="col-md-12 mt-3">
                  <label
                    htmlFor="validationCustomUsername"
                    className="form-label fw-normal text-muted"
                  >
                    School or University
                  </label>
                  <div className="input-group">
                    <select
                      className="form-select text-dark fw-normal py-2"
                      aria-label="Default select example"
                      onChange={(e) => setName(e.target.value)}
                      value={name}
                      name="name"
                    >
                      <option value="">Select Stream</option>
                      {streams.map((i, idx) => (
                        <option key={idx}>{i}</option>
                      ))}
                    </select>
                  </div>
                  {!name && isNotValid && (
                    <span className="text-danger">This field is required</span>
                  )}
                </div>
                <div className="col-md-12 mt-5">
                  <label
                    htmlFor="validationCustomUsername"
                    className="form-label fw-normal text-muted"
                  >
                    Precentage of Marks
                  </label>
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control fw-normal text-sece py-2"
                      placeholder="markes in percntage like 60.6%"
                      onChange={(e) => setScore(e.target.value)}
                      value={score}
                      name="score"
                    />
                  </div>
                  {!score && isNotValid && (
                    <span className="text-danger">This field is required</span>
                  )}

                  {!isUpdate && (
                    <button
                      className="btn btn-primary w-100 rounded-pill ms-1 py-2 mt-4 fs-5"
                      type="button"
                      onClick={() =>
                        addQualification({ name: name, score: score })
                      }
                    >
                      Add
                    </button>
                  )}

                  {isUpdate && (
                    <span>
                      <button
                        className="btn btn-primary w-20 rounded-pill ms-1 py-2 mt-4 fs-5"
                        type="button"
                        style={{ width: "30%" }}
                        onClick={() =>
                          updateQualification(isUpdate, {
                            name,
                            score,
                          })
                        }
                      >
                        Update
                      </button>
                      <button
                        style={{ width: "40%" }}
                        className="btn btn-info w-30  rounded-pill ms-1 py-2 mt-4 fs-5"
                        type="button"
                        onClick={setDefaults}
                      >
                        Cancel Update
                      </button>
                    </span>
                  )}
                </div>
              </form>
            </div>
            <div className="w-50 row rightblockMob">
              {userStreams &&
                userStreams.map((stream, idx) => (
                  <QualificationNode
                    key={idx}
                    data={stream}
                    id={stream.id}
                    onUpdate={() => updateItem(stream)}
                    onDelete={deleteItem}
                  />
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Qualification;
